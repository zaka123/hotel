-- MySQL dump 10.13  Distrib 8.2.0, for macos14.0 (arm64)
--
-- Host: localhost    Database: miranda
-- ------------------------------------------------------
-- Server version	8.2.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activations`
--

DROP TABLE IF EXISTS `activations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activations` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `code` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `completed_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activations_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activations`
--

LOCK TABLES `activations` WRITE;
/*!40000 ALTER TABLE `activations` DISABLE KEYS */;
INSERT INTO `activations` VALUES (1,1,'L1Hc42PKwwEPE0UoO2a9H6sQVLjauwQu',1,'2024-02-24 01:25:02','2024-02-24 01:25:02','2024-02-24 01:25:02'),(2,2,'kg1yX23qK9D5pBtYBpGs4x0JZOQEX3uN',1,'2024-02-24 01:25:02','2024-02-24 01:25:02','2024-02-24 01:25:02');
/*!40000 ALTER TABLE `activations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_notifications`
--

DROP TABLE IF EXISTS `admin_notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin_notifications` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `action_label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `action_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_notifications`
--

LOCK TABLES `admin_notifications` WRITE;
/*!40000 ALTER TABLE `admin_notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_histories`
--

DROP TABLE IF EXISTS `audit_histories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_histories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `module` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `request` longtext COLLATE utf8mb4_unicode_ci,
  `action` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reference_user` bigint unsigned NOT NULL,
  `reference_id` bigint unsigned NOT NULL,
  `reference_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `audit_histories_user_id_index` (`user_id`),
  KEY `audit_histories_module_index` (`module`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_histories`
--

LOCK TABLES `audit_histories` WRITE;
/*!40000 ALTER TABLE `audit_histories` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_histories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` bigint unsigned NOT NULL DEFAULT '0',
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `author_id` bigint unsigned DEFAULT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `icon` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` tinyint NOT NULL DEFAULT '0',
  `is_featured` tinyint NOT NULL DEFAULT '0',
  `is_default` tinyint unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categories_parent_id_index` (`parent_id`),
  KEY `categories_status_index` (`status`),
  KEY `categories_created_at_index` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'General',0,'Dolores ex quo possimus ad rerum ea voluptates eum. Minima qui corrupti quo at velit culpa voluptatem qui. Aut est voluptatem nihil quia facilis a. Debitis architecto debitis illo.','published',1,'Botble\\ACL\\Models\\User',NULL,0,0,1,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(2,'Hotel',0,'Rerum non voluptatem quia nostrum. Tenetur quod minus fugit velit sunt aut quae nulla. Qui aut repellat labore voluptates facere. Iste modi dolor nobis id ad.','published',1,'Botble\\ACL\\Models\\User',NULL,0,1,0,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(3,'Booking',0,'Eos aperiam ipsam cupiditate harum minus qui corrupti. Id omnis deleniti rerum eveniet aut ullam. Quas maxime similique accusantium maiores. Minima eveniet quis ut iste voluptates.','published',1,'Botble\\ACL\\Models\\User',NULL,0,1,0,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(4,'Resort',0,'Eius omnis omnis error qui deleniti id. Provident consequuntur facere qui magnam suscipit provident. Fugiat nemo dolor maiores et.','published',1,'Botble\\ACL\\Models\\User',NULL,0,1,0,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(5,'Travel',0,'Ratione molestiae nostrum error earum vero repellendus. Quia soluta praesentium maiores dolorem eos excepturi. Et sit id odit exercitationem. Consectetur et error minima voluptate et voluptas est.','published',1,'Botble\\ACL\\Models\\User',NULL,0,1,0,'2024-02-24 01:24:50','2024-02-24 01:24:50');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories_translations`
--

DROP TABLE IF EXISTS `categories_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories_translations` (
  `lang_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `categories_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lang_code`,`categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories_translations`
--

LOCK TABLES `categories_translations` WRITE;
/*!40000 ALTER TABLE `categories_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `categories_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_replies`
--

DROP TABLE IF EXISTS `contact_replies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contact_replies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_replies`
--

LOCK TABLES `contact_replies` WRITE;
/*!40000 ALTER TABLE `contact_replies` DISABLE KEYS */;
/*!40000 ALTER TABLE `contact_replies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'unread',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_widget_settings`
--

DROP TABLE IF EXISTS `dashboard_widget_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_widget_settings` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `user_id` bigint unsigned NOT NULL,
  `widget_id` bigint unsigned NOT NULL,
  `order` tinyint unsigned NOT NULL DEFAULT '0',
  `status` tinyint unsigned NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dashboard_widget_settings_user_id_index` (`user_id`),
  KEY `dashboard_widget_settings_widget_id_index` (`widget_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_widget_settings`
--

LOCK TABLES `dashboard_widget_settings` WRITE;
/*!40000 ALTER TABLE `dashboard_widget_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_widget_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dashboard_widgets`
--

DROP TABLE IF EXISTS `dashboard_widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dashboard_widgets` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dashboard_widgets`
--

LOCK TABLES `dashboard_widgets` WRITE;
/*!40000 ALTER TABLE `dashboard_widgets` DISABLE KEYS */;
/*!40000 ALTER TABLE `dashboard_widgets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries`
--

DROP TABLE IF EXISTS `galleries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `galleries` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_featured` tinyint unsigned NOT NULL DEFAULT '0',
  `order` tinyint unsigned NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `galleries_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries`
--

LOCK TABLES `galleries` WRITE;
/*!40000 ALTER TABLE `galleries` DISABLE KEYS */;
INSERT INTO `galleries` VALUES (1,'Duplex Restaurant','Est commodi aut eius est. Ipsa minima sunt harum est. Molestiae voluptas saepe eveniet et explicabo voluptatem aut voluptatem.',1,0,'galleries/01.jpg',1,'published','2024-02-24 01:25:01','2024-02-24 01:25:01'),(2,'Luxury room','Fugit harum sint praesentium quia et. At et animi sed accusamus iure optio tempore.',1,0,'galleries/02.jpg',1,'published','2024-02-24 01:25:01','2024-02-24 01:25:01'),(3,'Pacific Room','Velit velit saepe aut ut quis et numquam. Necessitatibus animi vero soluta necessitatibus unde.',1,0,'galleries/03.jpg',1,'published','2024-02-24 01:25:01','2024-02-24 01:25:01'),(4,'Family Room','Autem tempora ut quia consequatur dicta unde adipisci corporis. Ut amet nesciunt velit enim blanditiis ut. Inventore assumenda officia quo.',1,0,'galleries/04.jpg',1,'published','2024-02-24 01:25:01','2024-02-24 01:25:01'),(5,'King Bed','Ut odio est et quo natus maiores voluptate quia. Dignissimos nulla odio velit sint ab quo et. Magni quibusdam et nam temporibus alias ab dolorum.',1,0,'galleries/05.jpg',1,'published','2024-02-24 01:25:01','2024-02-24 01:25:01'),(6,'Special Foods','Ad dolorum minima error similique. Ut velit repellendus modi similique. In et qui autem ipsa corrupti harum est velit.',1,0,'galleries/06.jpg',1,'published','2024-02-24 01:25:01','2024-02-24 01:25:01');
/*!40000 ALTER TABLE `galleries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `galleries_translations`
--

DROP TABLE IF EXISTS `galleries_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `galleries_translations` (
  `lang_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `galleries_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lang_code`,`galleries_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `galleries_translations`
--

LOCK TABLES `galleries_translations` WRITE;
/*!40000 ALTER TABLE `galleries_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `galleries_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery_meta`
--

DROP TABLE IF EXISTS `gallery_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gallery_meta` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `images` text COLLATE utf8mb4_unicode_ci,
  `reference_id` bigint unsigned NOT NULL,
  `reference_type` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `gallery_meta_reference_id_index` (`reference_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery_meta`
--

LOCK TABLES `gallery_meta` WRITE;
/*!40000 ALTER TABLE `gallery_meta` DISABLE KEYS */;
INSERT INTO `gallery_meta` VALUES (1,'\"[{\\\"img\\\":\\\"galleries\\\\\\/1.jpg\\\",\\\"description\\\":\\\"Quisquam eaque dolorem iste aut. Non laboriosam molestiae voluptas earum molestias nam enim. Ex veritatis in dolorem sunt deleniti.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/2.jpg\\\",\\\"description\\\":\\\"Ut velit et aut cum assumenda. Quia magni quas et et laudantium optio. Ipsum ut non dolorem fuga repellendus.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/3.jpg\\\",\\\"description\\\":\\\"Quam accusantium sit a. Est quos magnam nobis accusantium est qui. Rerum ducimus harum qui in exercitationem est rerum.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/4.jpg\\\",\\\"description\\\":\\\"Nisi accusantium magnam quam expedita minus. Possimus nulla corporis rerum veritatis rem. Voluptatibus qui eos sapiente qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/5.jpg\\\",\\\"description\\\":\\\"Dolorem dolores odio et nemo. Eius sit nulla voluptatem numquam possimus laboriosam nihil.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/6.jpg\\\",\\\"description\\\":\\\"Quo neque enim quos quos omnis eos. Est quo officia sit sit ea molestiae nihil. Tempore suscipit iste sed officia id reiciendis unde.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/7.jpg\\\",\\\"description\\\":\\\"Quia enim officiis et enim rerum. Libero et sit magnam modi quia. Repellendus et nihil delectus ut molestias incidunt ut eaque.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/8.jpg\\\",\\\"description\\\":\\\"Blanditiis neque expedita hic dolorum architecto temporibus. Voluptates esse eligendi in rerum. Voluptas est tempora repudiandae fuga commodi.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/9.jpg\\\",\\\"description\\\":\\\"Unde perferendis quis consectetur provident. Vero laboriosam suscipit veniam deleniti rerum. Officiis rem ex ratione officiis sint quis qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/10.jpg\\\",\\\"description\\\":\\\"Eum similique aut reprehenderit est dolore quo quia. Voluptas natus neque cumque voluptate dolores iusto aut.\\\"}]\"',1,'Botble\\Gallery\\Models\\Gallery','2024-02-24 01:25:01','2024-02-24 01:25:01'),(2,'\"[{\\\"img\\\":\\\"galleries\\\\\\/1.jpg\\\",\\\"description\\\":\\\"Quisquam eaque dolorem iste aut. Non laboriosam molestiae voluptas earum molestias nam enim. Ex veritatis in dolorem sunt deleniti.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/2.jpg\\\",\\\"description\\\":\\\"Ut velit et aut cum assumenda. Quia magni quas et et laudantium optio. Ipsum ut non dolorem fuga repellendus.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/3.jpg\\\",\\\"description\\\":\\\"Quam accusantium sit a. Est quos magnam nobis accusantium est qui. Rerum ducimus harum qui in exercitationem est rerum.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/4.jpg\\\",\\\"description\\\":\\\"Nisi accusantium magnam quam expedita minus. Possimus nulla corporis rerum veritatis rem. Voluptatibus qui eos sapiente qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/5.jpg\\\",\\\"description\\\":\\\"Dolorem dolores odio et nemo. Eius sit nulla voluptatem numquam possimus laboriosam nihil.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/6.jpg\\\",\\\"description\\\":\\\"Quo neque enim quos quos omnis eos. Est quo officia sit sit ea molestiae nihil. Tempore suscipit iste sed officia id reiciendis unde.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/7.jpg\\\",\\\"description\\\":\\\"Quia enim officiis et enim rerum. Libero et sit magnam modi quia. Repellendus et nihil delectus ut molestias incidunt ut eaque.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/8.jpg\\\",\\\"description\\\":\\\"Blanditiis neque expedita hic dolorum architecto temporibus. Voluptates esse eligendi in rerum. Voluptas est tempora repudiandae fuga commodi.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/9.jpg\\\",\\\"description\\\":\\\"Unde perferendis quis consectetur provident. Vero laboriosam suscipit veniam deleniti rerum. Officiis rem ex ratione officiis sint quis qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/10.jpg\\\",\\\"description\\\":\\\"Eum similique aut reprehenderit est dolore quo quia. Voluptas natus neque cumque voluptate dolores iusto aut.\\\"}]\"',2,'Botble\\Gallery\\Models\\Gallery','2024-02-24 01:25:01','2024-02-24 01:25:01'),(3,'\"[{\\\"img\\\":\\\"galleries\\\\\\/1.jpg\\\",\\\"description\\\":\\\"Quisquam eaque dolorem iste aut. Non laboriosam molestiae voluptas earum molestias nam enim. Ex veritatis in dolorem sunt deleniti.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/2.jpg\\\",\\\"description\\\":\\\"Ut velit et aut cum assumenda. Quia magni quas et et laudantium optio. Ipsum ut non dolorem fuga repellendus.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/3.jpg\\\",\\\"description\\\":\\\"Quam accusantium sit a. Est quos magnam nobis accusantium est qui. Rerum ducimus harum qui in exercitationem est rerum.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/4.jpg\\\",\\\"description\\\":\\\"Nisi accusantium magnam quam expedita minus. Possimus nulla corporis rerum veritatis rem. Voluptatibus qui eos sapiente qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/5.jpg\\\",\\\"description\\\":\\\"Dolorem dolores odio et nemo. Eius sit nulla voluptatem numquam possimus laboriosam nihil.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/6.jpg\\\",\\\"description\\\":\\\"Quo neque enim quos quos omnis eos. Est quo officia sit sit ea molestiae nihil. Tempore suscipit iste sed officia id reiciendis unde.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/7.jpg\\\",\\\"description\\\":\\\"Quia enim officiis et enim rerum. Libero et sit magnam modi quia. Repellendus et nihil delectus ut molestias incidunt ut eaque.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/8.jpg\\\",\\\"description\\\":\\\"Blanditiis neque expedita hic dolorum architecto temporibus. Voluptates esse eligendi in rerum. Voluptas est tempora repudiandae fuga commodi.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/9.jpg\\\",\\\"description\\\":\\\"Unde perferendis quis consectetur provident. Vero laboriosam suscipit veniam deleniti rerum. Officiis rem ex ratione officiis sint quis qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/10.jpg\\\",\\\"description\\\":\\\"Eum similique aut reprehenderit est dolore quo quia. Voluptas natus neque cumque voluptate dolores iusto aut.\\\"}]\"',3,'Botble\\Gallery\\Models\\Gallery','2024-02-24 01:25:01','2024-02-24 01:25:01'),(4,'\"[{\\\"img\\\":\\\"galleries\\\\\\/1.jpg\\\",\\\"description\\\":\\\"Quisquam eaque dolorem iste aut. Non laboriosam molestiae voluptas earum molestias nam enim. Ex veritatis in dolorem sunt deleniti.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/2.jpg\\\",\\\"description\\\":\\\"Ut velit et aut cum assumenda. Quia magni quas et et laudantium optio. Ipsum ut non dolorem fuga repellendus.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/3.jpg\\\",\\\"description\\\":\\\"Quam accusantium sit a. Est quos magnam nobis accusantium est qui. Rerum ducimus harum qui in exercitationem est rerum.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/4.jpg\\\",\\\"description\\\":\\\"Nisi accusantium magnam quam expedita minus. Possimus nulla corporis rerum veritatis rem. Voluptatibus qui eos sapiente qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/5.jpg\\\",\\\"description\\\":\\\"Dolorem dolores odio et nemo. Eius sit nulla voluptatem numquam possimus laboriosam nihil.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/6.jpg\\\",\\\"description\\\":\\\"Quo neque enim quos quos omnis eos. Est quo officia sit sit ea molestiae nihil. Tempore suscipit iste sed officia id reiciendis unde.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/7.jpg\\\",\\\"description\\\":\\\"Quia enim officiis et enim rerum. Libero et sit magnam modi quia. Repellendus et nihil delectus ut molestias incidunt ut eaque.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/8.jpg\\\",\\\"description\\\":\\\"Blanditiis neque expedita hic dolorum architecto temporibus. Voluptates esse eligendi in rerum. Voluptas est tempora repudiandae fuga commodi.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/9.jpg\\\",\\\"description\\\":\\\"Unde perferendis quis consectetur provident. Vero laboriosam suscipit veniam deleniti rerum. Officiis rem ex ratione officiis sint quis qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/10.jpg\\\",\\\"description\\\":\\\"Eum similique aut reprehenderit est dolore quo quia. Voluptas natus neque cumque voluptate dolores iusto aut.\\\"}]\"',4,'Botble\\Gallery\\Models\\Gallery','2024-02-24 01:25:01','2024-02-24 01:25:01'),(5,'\"[{\\\"img\\\":\\\"galleries\\\\\\/1.jpg\\\",\\\"description\\\":\\\"Quisquam eaque dolorem iste aut. Non laboriosam molestiae voluptas earum molestias nam enim. Ex veritatis in dolorem sunt deleniti.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/2.jpg\\\",\\\"description\\\":\\\"Ut velit et aut cum assumenda. Quia magni quas et et laudantium optio. Ipsum ut non dolorem fuga repellendus.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/3.jpg\\\",\\\"description\\\":\\\"Quam accusantium sit a. Est quos magnam nobis accusantium est qui. Rerum ducimus harum qui in exercitationem est rerum.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/4.jpg\\\",\\\"description\\\":\\\"Nisi accusantium magnam quam expedita minus. Possimus nulla corporis rerum veritatis rem. Voluptatibus qui eos sapiente qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/5.jpg\\\",\\\"description\\\":\\\"Dolorem dolores odio et nemo. Eius sit nulla voluptatem numquam possimus laboriosam nihil.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/6.jpg\\\",\\\"description\\\":\\\"Quo neque enim quos quos omnis eos. Est quo officia sit sit ea molestiae nihil. Tempore suscipit iste sed officia id reiciendis unde.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/7.jpg\\\",\\\"description\\\":\\\"Quia enim officiis et enim rerum. Libero et sit magnam modi quia. Repellendus et nihil delectus ut molestias incidunt ut eaque.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/8.jpg\\\",\\\"description\\\":\\\"Blanditiis neque expedita hic dolorum architecto temporibus. Voluptates esse eligendi in rerum. Voluptas est tempora repudiandae fuga commodi.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/9.jpg\\\",\\\"description\\\":\\\"Unde perferendis quis consectetur provident. Vero laboriosam suscipit veniam deleniti rerum. Officiis rem ex ratione officiis sint quis qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/10.jpg\\\",\\\"description\\\":\\\"Eum similique aut reprehenderit est dolore quo quia. Voluptas natus neque cumque voluptate dolores iusto aut.\\\"}]\"',5,'Botble\\Gallery\\Models\\Gallery','2024-02-24 01:25:01','2024-02-24 01:25:01'),(6,'\"[{\\\"img\\\":\\\"galleries\\\\\\/1.jpg\\\",\\\"description\\\":\\\"Quisquam eaque dolorem iste aut. Non laboriosam molestiae voluptas earum molestias nam enim. Ex veritatis in dolorem sunt deleniti.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/2.jpg\\\",\\\"description\\\":\\\"Ut velit et aut cum assumenda. Quia magni quas et et laudantium optio. Ipsum ut non dolorem fuga repellendus.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/3.jpg\\\",\\\"description\\\":\\\"Quam accusantium sit a. Est quos magnam nobis accusantium est qui. Rerum ducimus harum qui in exercitationem est rerum.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/4.jpg\\\",\\\"description\\\":\\\"Nisi accusantium magnam quam expedita minus. Possimus nulla corporis rerum veritatis rem. Voluptatibus qui eos sapiente qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/5.jpg\\\",\\\"description\\\":\\\"Dolorem dolores odio et nemo. Eius sit nulla voluptatem numquam possimus laboriosam nihil.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/6.jpg\\\",\\\"description\\\":\\\"Quo neque enim quos quos omnis eos. Est quo officia sit sit ea molestiae nihil. Tempore suscipit iste sed officia id reiciendis unde.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/7.jpg\\\",\\\"description\\\":\\\"Quia enim officiis et enim rerum. Libero et sit magnam modi quia. Repellendus et nihil delectus ut molestias incidunt ut eaque.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/8.jpg\\\",\\\"description\\\":\\\"Blanditiis neque expedita hic dolorum architecto temporibus. Voluptates esse eligendi in rerum. Voluptas est tempora repudiandae fuga commodi.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/9.jpg\\\",\\\"description\\\":\\\"Unde perferendis quis consectetur provident. Vero laboriosam suscipit veniam deleniti rerum. Officiis rem ex ratione officiis sint quis qui.\\\"},{\\\"img\\\":\\\"galleries\\\\\\/10.jpg\\\",\\\"description\\\":\\\"Eum similique aut reprehenderit est dolore quo quia. Voluptas natus neque cumque voluptate dolores iusto aut.\\\"}]\"',6,'Botble\\Gallery\\Models\\Gallery','2024-02-24 01:25:01','2024-02-24 01:25:01');
/*!40000 ALTER TABLE `gallery_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery_meta_translations`
--

DROP TABLE IF EXISTS `gallery_meta_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `gallery_meta_translations` (
  `lang_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gallery_meta_id` bigint unsigned NOT NULL,
  `images` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lang_code`,`gallery_meta_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery_meta_translations`
--

LOCK TABLES `gallery_meta_translations` WRITE;
/*!40000 ALTER TABLE `gallery_meta_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_meta_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_amenities`
--

DROP TABLE IF EXISTS `ht_amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_amenities` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_amenities`
--

LOCK TABLES `ht_amenities` WRITE;
/*!40000 ALTER TABLE `ht_amenities` DISABLE KEYS */;
INSERT INTO `ht_amenities` VALUES (1,'Air conditioner','fal fa-bath','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(2,'High speed WiFi','fal fa-wifi','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(3,'Strong Locker','fal fa-key','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(4,'Breakfast','fal fa-cut','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(5,'Kitchen','fal fa-guitar','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(6,'Smart Security','fal fa-lock','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(7,'Cleaning','fal fa-broom','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(8,'Shower','fal fa-shower','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(9,'24/7 Online Support','fal fa-headphones-alt','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(10,'Grocery','fal fa-shopping-basket','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(11,'Single bed','fal fa-bed','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(12,'Expert Team','fal fa-users','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(13,'Shop near','fal fa-shopping-cart','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(14,'Towels','fal fa-bus','published','2024-02-24 01:24:50','2024-02-24 01:24:50');
/*!40000 ALTER TABLE `ht_amenities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_amenities_translations`
--

DROP TABLE IF EXISTS `ht_amenities_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_amenities_translations` (
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ht_amenities_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lang_code`,`ht_amenities_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_amenities_translations`
--

LOCK TABLES `ht_amenities_translations` WRITE;
/*!40000 ALTER TABLE `ht_amenities_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_amenities_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_booking_addresses`
--

DROP TABLE IF EXISTS `ht_booking_addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_booking_addresses` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `booking_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_booking_addresses`
--

LOCK TABLES `ht_booking_addresses` WRITE;
/*!40000 ALTER TABLE `ht_booking_addresses` DISABLE KEYS */;
INSERT INTO `ht_booking_addresses` VALUES (1,'Rafael','Marvin','307.693.4954','forest94@example.org','Guyana','Lake Flavioland','Lake Clotildemouth','95795-6474','4722 Lauriane Hollow\nWest Dino, NM 93388',1,'2024-02-24 01:25:04','2024-02-24 01:25:04'),(2,'Pete','Witting','423.661.3975','parker.brendan@example.net','Cook Islands','Johnathantown','Port Mariela','81929-4239','40180 Katrine Landing Apt. 857\nHeidenreichtown, MD 88575-4714',2,'2024-02-24 01:25:04','2024-02-24 01:25:04'),(3,'Lonnie','Emmerich','415.952.5773','delfina27@example.net','India','Rivermouth','Emelieport','84682-4012','835 Champlin Lane Apt. 431\nNew Cordeliafort, KY 87661',3,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(4,'Cletus','Shields','+15095418977','myrl04@example.org','Paraguay','New Savanna','New Ricky','15773','2507 Lexi Turnpike Suite 884\nMyleneton, PA 86288-2835',4,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(5,'Stanley','Tillman','262-307-9524','silas25@example.net','United States Minor Outlying Islands','Coralieberg','Lake Eloiseland','25005-2126','2867 Estel Track Suite 454\nSouth Lavinabury, CT 02178',5,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(6,'Lindsey','O\'Keefe','(541) 863-5795','rudy51@example.com','Uzbekistan','Veumburgh','Port Kip','53953-3758','1185 Metz Heights Apt. 112\nLake Raheembury, MS 84681',6,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(7,'Agustina','Ernser','+1 (539) 444-2444','leannon.nia@example.org','Mongolia','McCulloughberg','Sonyachester','36985','2600 Block Views\nWest Wileymouth, TN 35887',7,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(8,'Victor','D\'Amore','+16516708854','davion18@example.net','Finland','Wildermanville','Greenfelderhaven','74888-0268','626 Keaton Cliff Suite 400\nDestinyberg, NM 39860',8,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(9,'Zane','Bergnaum','940-720-6179','greyson06@example.com','Cote d\'Ivoire','Christopmouth','New Lavernaburgh','12101-8476','494 Wiza Lodge\nPort Itzelchester, MA 25333',9,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(10,'Magnus','Orn','240.402.0674','garret.trantow@example.org','Palau','Claudmouth','Marjoriemouth','88083-2029','48359 Maximillia Trail Suite 803\nWest Tyson, NE 92766',10,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(11,'Freddie','Robel','364-821-5855','jodie60@example.com','Bulgaria','South Henry','Kshlerinview','14906','3378 Blanda Club Suite 658\nNew Rowland, VA 22689',11,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(12,'Sven','Goldner','564.924.1030','jwhite@example.net','Togo','North Ellis','South Rosettaborough','50692','21107 Walsh Stravenue Apt. 787\nMuraziktown, PA 16511',12,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(13,'Daren','Runolfsson','+1-817-605-9590','violette.wisoky@example.com','Morocco','North Joanneview','North Deshaun','66283','89041 Fritsch Ridge\nWeissnatchester, IN 33718',13,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(14,'Cameron','Abbott','1-610-649-0601','johns.juana@example.com','Angola','Mrazland','Raustad','39226-1117','8461 Berge Rapid Apt. 880\nWest Ona, DE 10903-6796',14,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(15,'Haven','Luettgen','(910) 812-5613','ricky.batz@example.com','Guam','North Scot','North Amy','35693','772 Gleichner Overpass\nFrancomouth, AL 84626',15,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(16,'Osvaldo','King','1-330-258-9427','ferne.hartmann@example.com','Lithuania','Port Kevenport','Jacobsonchester','97698','225 Huels Port Suite 064\nLake Seamus, MI 25250-3008',16,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(17,'Zack','Koepp','765.736.5845','eblanda@example.net','Cambodia','Port Ibrahimbury','North Leliahaven','95543','99112 Volkman Plaza Apt. 393\nJoelleville, MN 32101',17,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(18,'Kaden','Doyle','+1.732.621.1487','elenor.casper@example.com','Bahrain','Lake Russell','Sawaynhaven','72872','39261 Amira Ford\nWillport, AR 43897',18,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(19,'Kurt','Buckridge','351.753.6231','beverly.gibson@example.com','Norfolk Island','Casandrastad','Mauriciofort','41626-5548','92988 Cormier Tunnel Apt. 117\nEstrellabury, MN 83375',19,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(20,'Evalyn','Kulas','(623) 688-3454','mcorkery@example.net','Peru','Isaacfort','North Citlalli','72615-6684','1770 Jones Ridge\nMosciskiside, AK 81372',20,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(21,'Mozelle','Larson','+19415153350','ernser.jesus@example.net','Iraq','Cristside','Casperside','42845','35148 Arianna Divide\nWest Lesley, AK 36414-4629',21,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(22,'Emmanuelle','Bradtke','+1-334-418-5287','wintheiser.jody@example.org','Slovenia','Hicklebury','North Ahmadborough','85989','29165 Metz Village\nReichelshire, LA 83020-6718',22,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(23,'Andy','Kuhn','443-880-5958','destiney73@example.org','Madagascar','North Davonteland','East Skyefort','00688','9196 Cullen Shores\nEast Jimmyland, WA 12970-0970',23,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(24,'Jovany','Hoppe','(229) 407-4166','paucek.taryn@example.net','Kiribati','Elnaburgh','Pattieside','24711','9875 Kale Forge Suite 413\nReingerview, TX 07257',24,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(25,'Sally','Watsica','743.447.1564','powlowski.armando@example.net','Mauritania','West Everardoview','New Yessenia','55892','3134 Brady Corners Apt. 439\nLake Marlen, NE 16920-2348',25,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(26,'Maximilian','Reilly','+17624456768','raynor.dorian@example.org','Cape Verde','McDermottchester','Mertzfurt','10867','246 Fay Center Apt. 244\nCliftonmouth, NJ 84077-3367',26,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(27,'Cyril','Williamson','+1-534-640-8835','paucek.demetris@example.net','British Indian Ocean Territory (Chagos Archipelago)','Wintheiserbury','Port Brielle','64777-0142','27267 Faustino Shores Suite 550\nEast Rebeca, DC 50693',27,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(28,'Dayton','D\'Amore','620.287.8523','madelyn.quitzon@example.org','South Africa','North Maeshire','Christophefurt','38302','33071 Green Point\nBrionnamouth, FL 54674',28,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(29,'Eudora','Becker','+1 (657) 348-4734','eugene17@example.org','Singapore','New Bentown','Nyaview','88666-8054','7905 Llewellyn Garden\nRyleyton, NE 91200-3183',29,'2024-02-24 01:25:05','2024-02-24 01:25:05');
/*!40000 ALTER TABLE `ht_booking_addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_booking_rooms`
--

DROP TABLE IF EXISTS `ht_booking_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_booking_rooms` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint unsigned NOT NULL,
  `room_id` bigint unsigned DEFAULT NULL,
  `room_image` text COLLATE utf8mb4_unicode_ci,
  `room_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` decimal(15,2) NOT NULL,
  `currency_id` bigint unsigned DEFAULT NULL,
  `number_of_rooms` int NOT NULL DEFAULT '1',
  `start_date` date NOT NULL,
  `end_date` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_booking_rooms`
--

LOCK TABLES `ht_booking_rooms` WRITE;
/*!40000 ALTER TABLE `ht_booking_rooms` DISABLE KEYS */;
INSERT INTO `ht_booking_rooms` VALUES (1,1,1,'rooms/01.jpg','Luxury Hall Of Fame',140.00,NULL,1,'2024-02-01','2024-02-03','2024-02-24 01:25:04','2024-02-24 01:25:04'),(2,2,5,'rooms/05.jpg','Family Suite',110.00,NULL,1,'2024-02-02','2024-02-04','2024-02-24 01:25:04','2024-02-24 01:25:04'),(3,3,5,'rooms/05.jpg','Family Suite',110.00,NULL,2,'2024-02-03','2024-02-04','2024-02-24 01:25:05','2024-02-24 01:25:05'),(4,4,4,'rooms/04.jpg','Junior Suite',135.00,NULL,2,'2024-02-04','2024-02-06','2024-02-24 01:25:05','2024-02-24 01:25:05'),(5,5,3,'rooms/03.jpg','Pacific Room',196.00,NULL,3,'2024-02-05','2024-02-06','2024-02-24 01:25:05','2024-02-24 01:25:05'),(6,6,1,'rooms/01.jpg','Luxury Hall Of Fame',140.00,NULL,3,'2024-02-06','2024-02-08','2024-02-24 01:25:05','2024-02-24 01:25:05'),(7,7,5,'rooms/05.jpg','Family Suite',110.00,NULL,1,'2024-02-07','2024-02-10','2024-02-24 01:25:05','2024-02-24 01:25:05'),(8,8,4,'rooms/04.jpg','Junior Suite',135.00,NULL,1,'2024-02-08','2024-02-09','2024-02-24 01:25:05','2024-02-24 01:25:05'),(9,9,4,'rooms/04.jpg','Junior Suite',135.00,NULL,2,'2024-02-09','2024-02-12','2024-02-24 01:25:05','2024-02-24 01:25:05'),(10,10,8,'rooms/02.jpg','President Room',184.00,NULL,3,'2024-02-10','2024-02-12','2024-02-24 01:25:05','2024-02-24 01:25:05'),(11,11,4,'rooms/04.jpg','Junior Suite',135.00,NULL,3,'2024-02-11','2024-02-14','2024-02-24 01:25:05','2024-02-24 01:25:05'),(12,12,3,'rooms/03.jpg','Pacific Room',196.00,NULL,1,'2024-02-12','2024-02-13','2024-02-24 01:25:05','2024-02-24 01:25:05'),(13,13,6,'rooms/06.jpg','Relax Suite',114.00,NULL,3,'2024-02-13','2024-02-16','2024-02-24 01:25:05','2024-02-24 01:25:05'),(14,14,3,'rooms/03.jpg','Pacific Room',196.00,NULL,2,'2024-02-14','2024-02-15','2024-02-24 01:25:05','2024-02-24 01:25:05'),(15,15,7,'rooms/01.jpg','Luxury Suite',117.00,NULL,2,'2024-02-15','2024-02-16','2024-02-24 01:25:05','2024-02-24 01:25:05'),(16,16,7,'rooms/01.jpg','Luxury Suite',117.00,NULL,3,'2024-02-16','2024-02-17','2024-02-24 01:25:05','2024-02-24 01:25:05'),(17,17,6,'rooms/06.jpg','Relax Suite',114.00,NULL,3,'2024-02-17','2024-02-20','2024-02-24 01:25:05','2024-02-24 01:25:05'),(18,18,6,'rooms/06.jpg','Relax Suite',114.00,NULL,3,'2024-02-18','2024-02-19','2024-02-24 01:25:05','2024-02-24 01:25:05'),(19,19,2,'rooms/02.jpg','Pendora Fame',183.00,NULL,1,'2024-02-19','2024-02-22','2024-02-24 01:25:05','2024-02-24 01:25:05'),(20,20,1,'rooms/01.jpg','Luxury Hall Of Fame',140.00,NULL,3,'2024-02-20','2024-02-23','2024-02-24 01:25:05','2024-02-24 01:25:05'),(21,21,8,'rooms/02.jpg','President Room',184.00,NULL,1,'2024-02-21','2024-02-24','2024-02-24 01:25:05','2024-02-24 01:25:05'),(22,22,3,'rooms/03.jpg','Pacific Room',196.00,NULL,2,'2024-02-22','2024-02-23','2024-02-24 01:25:05','2024-02-24 01:25:05'),(23,23,6,'rooms/06.jpg','Relax Suite',114.00,NULL,3,'2024-02-23','2024-02-24','2024-02-24 01:25:05','2024-02-24 01:25:05'),(24,24,1,'rooms/01.jpg','Luxury Hall Of Fame',140.00,NULL,3,'2024-02-24','2024-02-27','2024-02-24 01:25:05','2024-02-24 01:25:05'),(25,25,2,'rooms/02.jpg','Pendora Fame',183.00,NULL,3,'2024-02-25','2024-02-27','2024-02-24 01:25:05','2024-02-24 01:25:05'),(26,26,2,'rooms/02.jpg','Pendora Fame',183.00,NULL,1,'2024-02-26','2024-02-29','2024-02-24 01:25:05','2024-02-24 01:25:05'),(27,27,4,'rooms/04.jpg','Junior Suite',135.00,NULL,2,'2024-02-27','2024-02-29','2024-02-24 01:25:05','2024-02-24 01:25:05'),(28,28,5,'rooms/05.jpg','Family Suite',110.00,NULL,3,'2024-02-28','2024-03-01','2024-02-24 01:25:05','2024-02-24 01:25:05'),(29,29,4,'rooms/04.jpg','Junior Suite',135.00,NULL,3,'2024-02-29','2024-03-02','2024-02-24 01:25:05','2024-02-24 01:25:05');
/*!40000 ALTER TABLE `ht_booking_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_booking_services`
--

DROP TABLE IF EXISTS `ht_booking_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_booking_services` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint unsigned NOT NULL,
  `service_id` bigint unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_booking_services`
--

LOCK TABLES `ht_booking_services` WRITE;
/*!40000 ALTER TABLE `ht_booking_services` DISABLE KEYS */;
INSERT INTO `ht_booking_services` VALUES (1,1,6),(2,2,4),(3,3,2),(4,4,5),(5,5,5),(6,6,6),(7,7,3),(8,8,2),(9,9,5),(10,10,3),(11,11,2),(12,12,3),(13,13,3),(14,14,4),(15,15,1),(16,16,3),(17,17,6),(18,18,3),(19,19,4),(20,20,4),(21,21,4),(22,22,6),(23,23,2),(24,24,3),(25,25,2),(26,26,4),(27,27,4),(28,28,1),(29,29,1);
/*!40000 ALTER TABLE `ht_booking_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_bookings`
--

DROP TABLE IF EXISTS `ht_bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_bookings` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `amount` decimal(15,2) NOT NULL,
  `sub_total` decimal(15,2) unsigned NOT NULL,
  `coupon_amount` decimal(15,2) unsigned NOT NULL DEFAULT '0.00',
  `coupon_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tax_amount` decimal(15,2) NOT NULL,
  `currency_id` bigint unsigned DEFAULT NULL,
  `requests` text COLLATE utf8mb4_unicode_ci,
  `arrival_time` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_of_guests` int DEFAULT NULL,
  `payment_id` bigint unsigned DEFAULT NULL,
  `customer_id` bigint unsigned DEFAULT NULL,
  `transaction_id` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_bookings`
--

LOCK TABLES `ht_bookings` WRITE;
/*!40000 ALTER TABLE `ht_bookings` DISABLE KEYS */;
INSERT INTO `ht_bookings` VALUES (1,140.00,140.00,0.00,NULL,0.00,NULL,'Nemo corporis consequatur assumenda placeat voluptatem exercitationem quo.',NULL,3,1,5,'fidqykGp7o0aR26OTk3i','cancelled','2024-02-24 01:25:04','2024-02-24 01:25:04'),(2,110.00,110.00,0.00,NULL,0.00,NULL,'Aliquam provident itaque est perspiciatis.',NULL,3,2,9,'TGiERDfLgpvzqD57bUBx','pending','2024-02-24 01:25:04','2024-02-24 01:25:04'),(3,220.00,220.00,0.00,NULL,0.00,NULL,'Temporibus non aut explicabo.',NULL,2,3,2,'SCen5brsJUtb7bLB8c7W','processing','2024-02-24 01:25:05','2024-02-24 01:25:05'),(4,270.00,270.00,0.00,NULL,0.00,NULL,'Alias minima qui enim rem quas at.',NULL,2,4,7,'qm3ObzBfDON65eleFIDC','cancelled','2024-02-24 01:25:05','2024-02-24 01:25:05'),(5,588.00,588.00,0.00,NULL,0.00,NULL,'Eius architecto qui enim velit ipsam nisi a.',NULL,3,5,10,'wWpjwZFfxn4xeUmWR6wU','cancelled','2024-02-24 01:25:05','2024-02-24 01:25:05'),(6,420.00,420.00,0.00,NULL,0.00,NULL,'Id accusantium temporibus qui beatae.',NULL,9,6,4,'N0ZouCGvlcEQK09muDZo','pending','2024-02-24 01:25:05','2024-02-24 01:25:05'),(7,110.00,110.00,0.00,NULL,0.00,NULL,'Doloremque nisi sit porro sit dolor.',NULL,3,7,9,'qHkAOgOWD3WanIYxiooz','processing','2024-02-24 01:25:05','2024-02-24 01:25:05'),(8,135.00,135.00,0.00,NULL,0.00,NULL,'Magni et minima sapiente assumenda.',NULL,1,8,4,'536lzNM9hqixlyGqDrtX','pending','2024-02-24 01:25:05','2024-02-24 01:25:05'),(9,270.00,270.00,0.00,NULL,0.00,NULL,'Modi in expedita sapiente asperiores sunt reiciendis et.',NULL,4,9,9,'hy9GmU8I5KGUSnjWtuzU','pending','2024-02-24 01:25:05','2024-02-24 01:25:05'),(10,552.00,552.00,0.00,NULL,0.00,NULL,'Veniam modi nihil minima sit.',NULL,6,10,11,'aON5qtvpuIwMsR4y5rQ1','pending','2024-02-24 01:25:05','2024-02-24 01:25:05'),(11,405.00,405.00,0.00,NULL,0.00,NULL,'Inventore officiis error omnis magnam corrupti perspiciatis explicabo.',NULL,6,11,2,'d0H0YmD4GAHy8TWm8YBv','processing','2024-02-24 01:25:05','2024-02-24 01:25:05'),(12,196.00,196.00,0.00,NULL,0.00,NULL,'Rem culpa atque ad magnam laborum molestiae.',NULL,1,12,3,'GtSs54OwbZzWsnsfaGUK','processing','2024-02-24 01:25:05','2024-02-24 01:25:05'),(13,342.00,342.00,0.00,NULL,0.00,NULL,'Nemo voluptas praesentium excepturi voluptatum dolor ipsa vel nam.',NULL,3,13,3,'9CyGkx8mzLMvTkKszjZ1','cancelled','2024-02-24 01:25:05','2024-02-24 01:25:05'),(14,392.00,392.00,0.00,NULL,0.00,NULL,'Quaerat est enim sint facere saepe voluptas dignissimos.',NULL,6,14,10,'z5DdFElaqWfAus7QJaQN','pending','2024-02-24 01:25:05','2024-02-24 01:25:05'),(15,234.00,234.00,0.00,NULL,0.00,NULL,'Doloribus dicta impedit accusantium quisquam architecto omnis quia.',NULL,4,15,4,'VEXa84zTZagU6QbSvUEZ','completed','2024-02-24 01:25:05','2024-02-24 01:25:05'),(16,351.00,351.00,0.00,NULL,0.00,NULL,'Voluptas consequatur est expedita velit.',NULL,9,16,5,'yvS4uctg9Pl66aVRH2AR','processing','2024-02-24 01:25:05','2024-02-24 01:25:05'),(17,342.00,342.00,0.00,NULL,0.00,NULL,'Nemo voluptas ipsa sed rerum natus.',NULL,6,17,6,'7P7qFv1I4X0EDEa9Ux6O','completed','2024-02-24 01:25:05','2024-02-24 01:25:05'),(18,342.00,342.00,0.00,NULL,0.00,NULL,'Et delectus aut rem nobis cupiditate.',NULL,3,18,4,'3AmBiRnm2FO61r9N2cGy','processing','2024-02-24 01:25:05','2024-02-24 01:25:05'),(19,183.00,183.00,0.00,NULL,0.00,NULL,'Quam accusamus sint perferendis voluptas voluptas provident tempore.',NULL,2,19,6,'qPx471VFojUvZgHV0N2g','processing','2024-02-24 01:25:05','2024-02-24 01:25:05'),(20,420.00,420.00,0.00,NULL,0.00,NULL,'Voluptatem pariatur ducimus doloribus ut magnam laudantium a qui.',NULL,6,20,4,'ygKtYVUyWCvF8DzuPCJ9','processing','2024-02-24 01:25:05','2024-02-24 01:25:05'),(21,184.00,184.00,0.00,NULL,0.00,NULL,'Recusandae repudiandae dolores perferendis recusandae qui suscipit eum.',NULL,3,21,7,'PDNZa6aP97nKHf2Ez0Ad','completed','2024-02-24 01:25:05','2024-02-24 01:25:05'),(22,392.00,392.00,0.00,NULL,0.00,NULL,'Et nesciunt explicabo vel tempore.',NULL,2,22,10,'m12kggXxDtwO6NfgfwfO','pending','2024-02-24 01:25:05','2024-02-24 01:25:05'),(23,342.00,342.00,0.00,NULL,0.00,NULL,'Perspiciatis ipsa repellat est.',NULL,6,23,8,'s7D4jVyOIydfQzufjXrq','cancelled','2024-02-24 01:25:05','2024-02-24 01:25:05'),(24,420.00,420.00,0.00,NULL,0.00,NULL,'Placeat cum modi sunt fugiat debitis exercitationem reiciendis.',NULL,6,24,4,'JqqUbWPcM8YXTmo7vaN8','processing','2024-02-24 01:25:05','2024-02-24 01:25:05'),(25,549.00,549.00,0.00,NULL,0.00,NULL,'Voluptatem et temporibus cumque cum saepe aut.',NULL,6,25,8,'iqLIuap3Gdw60ksaPZc5','cancelled','2024-02-24 01:25:05','2024-02-24 01:25:05'),(26,183.00,183.00,0.00,NULL,0.00,NULL,'Quos eum velit inventore tempora dolorum ratione ducimus.',NULL,1,26,9,'kyTs4Ud7ElLa6VVGjQK1','cancelled','2024-02-24 01:25:05','2024-02-24 01:25:05'),(27,270.00,270.00,0.00,NULL,0.00,NULL,'Cum voluptate veritatis in.',NULL,4,27,9,'0EB78lbUpephSPE8ojfO','processing','2024-02-24 01:25:05','2024-02-24 01:25:05'),(28,330.00,330.00,0.00,NULL,0.00,NULL,'Doloribus ut aut quasi nam omnis eos.',NULL,9,28,4,'cqL6OINsP6j3OTzKOlGG','pending','2024-02-24 01:25:05','2024-02-24 01:25:05'),(29,405.00,405.00,0.00,NULL,0.00,NULL,'Magnam occaecati placeat mollitia.',NULL,6,29,8,'omS85DIsgJdsV1LxEI0M','completed','2024-02-24 01:25:05','2024-02-24 01:25:05');
/*!40000 ALTER TABLE `ht_bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_coupons`
--

DROP TABLE IF EXISTS `ht_coupons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_coupons` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` decimal(8,2) NOT NULL,
  `quantity` int DEFAULT NULL,
  `total_used` int unsigned NOT NULL DEFAULT '0',
  `expires_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ht_coupons_code_unique` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_coupons`
--

LOCK TABLES `ht_coupons` WRITE;
/*!40000 ALTER TABLE `ht_coupons` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_coupons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_currencies`
--

DROP TABLE IF EXISTS `ht_currencies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_currencies` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `symbol` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_prefix_symbol` tinyint unsigned NOT NULL DEFAULT '0',
  `decimals` tinyint unsigned NOT NULL DEFAULT '0',
  `order` int unsigned NOT NULL DEFAULT '0',
  `is_default` tinyint NOT NULL DEFAULT '0',
  `exchange_rate` double NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_currencies`
--

LOCK TABLES `ht_currencies` WRITE;
/*!40000 ALTER TABLE `ht_currencies` DISABLE KEYS */;
INSERT INTO `ht_currencies` VALUES (1,'USD','$',1,2,0,1,1,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(2,'EUR','€',0,2,1,0,0.91,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(3,'VND','₫',0,0,2,0,23717.5,'2024-02-24 01:24:50','2024-02-24 01:24:50');
/*!40000 ALTER TABLE `ht_currencies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_customer_password_resets`
--

DROP TABLE IF EXISTS `ht_customer_password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_customer_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_customer_password_resets`
--

LOCK TABLES `ht_customer_password_resets` WRITE;
/*!40000 ALTER TABLE `ht_customer_password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_customer_password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_customers`
--

DROP TABLE IF EXISTS `ht_customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_customers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `phone` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `zip` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `state` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ht_customers_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_customers`
--

LOCK TABLES `ht_customers` WRITE;
/*!40000 ALTER TABLE `ht_customers` DISABLE KEYS */;
INSERT INTO `ht_customers` VALUES (1,'Caroline','Rempel','bvandervort@example.net','$2y$12$Q.uagZa9P.KRZbBEkP24u.YAnMgtIssHwAe/wn8iqB3ZqkJGuWWZ.','customers/1.jpg',NULL,'+13643045236',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL),(2,'Laverne','Paucek','pmurazik@example.org','$2y$12$xbIMgyo90XUS4XWF4sgsmOHav7zYStA70bVXxHiJiNf9rUyu3u2/m','customers/2.jpg',NULL,'+15648693989',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL),(3,'Ruthie','Prosacco','marilyne.parisian@example.com','$2y$12$tqntZf8q/qUpnM.4xTYk8eK0IhFjBAjDayCPsCapxOdLKTR0DgQuO','customers/3.jpg',NULL,'+15626892288',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL),(4,'Oral','Reinger','qbrekke@example.com','$2y$12$TtfPrNeDJH0ws25OJFKN3OIrneqcQSCwlGDTg.0ijtW9AelTpj6i6','customers/4.jpg',NULL,'+13392369694',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL),(5,'Judge','Lakin','marjolaine87@example.org','$2y$12$HD4Y.5EF5Nx8GTOvvYI5XefCkU/5EMjOaMkaJnWYQNKXC06dr4doe','customers/5.jpg',NULL,'+15638760104',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL),(6,'Taryn','Dietrich','ernser.bethany@example.org','$2y$12$0zJ5ornwslDBsbdUO7yLS.2bpe2GPgUte.OxOBe2gjR4MmbmkIsVS','customers/6.jpg',NULL,'+14128010216',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL),(7,'Camille','Cassin','addie.zulauf@example.net','$2y$12$saad1GM9LC0lZj7jepYZjODHEoesmx3E9xuZE7EGNHcXWf8P7MJ4K','customers/7.jpg',NULL,'+18505753187',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL),(8,'Lura','Pfannerstill','mgoodwin@example.com','$2y$12$BnTFds8BxvG/LAaQcq7TBO2tDkqvosI7G9/eDdxiu2QRmf6ngsOOO','customers/8.jpg',NULL,'+17175491271',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL),(9,'Ettie','Lehner','isabelle.hayes@example.net','$2y$12$SW/qpBlvcrP57rOHTtNa.eyCRfScin6pGzwudsioTrHCPoROyt28a','customers/9.jpg',NULL,'+19136227699',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL),(10,'Daphney','Jerde','akertzmann@example.com','$2y$12$3HnxfJ4ImgV0yxAdsKo5DOxN7kJ3O54KIPAntXQnKELpVt8glDXWW','customers/10.jpg',NULL,'+19796665135',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL),(11,'Josh','Johnson','customer@archielite.com','$2y$12$Ham9cUprCJUC9YlB8.xE9.C1oUwN0SreMQPR.bld4F.1WRQfmr0fm','customers/6.jpg',NULL,'+16179083469',NULL,NULL,NULL,NULL,NULL,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL,NULL);
/*!40000 ALTER TABLE `ht_customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_features`
--

DROP TABLE IF EXISTS `ht_features`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_features` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `icon` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_features`
--

LOCK TABLES `ht_features` WRITE;
/*!40000 ALTER TABLE `ht_features` DISABLE KEYS */;
INSERT INTO `ht_features` VALUES (1,'Have High Rating','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','flaticon-rating',1,'published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(2,'Quiet Hours','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','flaticon-clock',1,'published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(3,'Best Locations','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','flaticon-location-pin',1,'published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(4,'Free Cancellation','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','flaticon-clock-1',0,'published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(5,'Payment Options','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','flaticon-credit-card',0,'published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(6,'Special Offers','Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.','flaticon-discount',0,'published','2024-02-24 01:24:53','2024-02-24 01:24:53');
/*!40000 ALTER TABLE `ht_features` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_features_translations`
--

DROP TABLE IF EXISTS `ht_features_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_features_translations` (
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ht_features_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lang_code`,`ht_features_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_features_translations`
--

LOCK TABLES `ht_features_translations` WRITE;
/*!40000 ALTER TABLE `ht_features_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_features_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_food_types`
--

DROP TABLE IF EXISTS `ht_food_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_food_types` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_food_types`
--

LOCK TABLES `ht_food_types` WRITE;
/*!40000 ALTER TABLE `ht_food_types` DISABLE KEYS */;
INSERT INTO `ht_food_types` VALUES (1,'Chicken','flaticon-boiled','published','2024-02-24 01:24:51','2024-02-24 01:24:51'),(2,'Italian','flaticon-pizza','published','2024-02-24 01:24:51','2024-02-24 01:24:51'),(3,'Coffee','flaticon-coffee','published','2024-02-24 01:24:51','2024-02-24 01:24:51'),(4,'Bake Cake','flaticon-cake','published','2024-02-24 01:24:51','2024-02-24 01:24:51'),(5,'Cookies','flaticon-cookie','published','2024-02-24 01:24:51','2024-02-24 01:24:51'),(6,'Cocktail','flaticon-cocktail','published','2024-02-24 01:24:51','2024-02-24 01:24:51');
/*!40000 ALTER TABLE `ht_food_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_food_types_translations`
--

DROP TABLE IF EXISTS `ht_food_types_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_food_types_translations` (
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ht_food_types_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lang_code`,`ht_food_types_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_food_types_translations`
--

LOCK TABLES `ht_food_types_translations` WRITE;
/*!40000 ALTER TABLE `ht_food_types_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_food_types_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_foods`
--

DROP TABLE IF EXISTS `ht_foods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_foods` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(15,0) unsigned DEFAULT NULL,
  `currency_id` bigint unsigned DEFAULT NULL,
  `food_type_id` bigint unsigned NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_foods`
--

LOCK TABLES `ht_foods` WRITE;
/*!40000 ALTER TABLE `ht_foods` DISABLE KEYS */;
INSERT INTO `ht_foods` VALUES (1,'Eggs &amp; Bacon','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',161,NULL,1,'foods/01.jpg','published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(2,'Tea or Coffee','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',106,NULL,1,'foods/02.jpg','published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(3,'Chia Oatmeal','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',144,NULL,1,'foods/03.jpg','published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(4,'Juice','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',178,NULL,1,'foods/04.jpg','published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(5,'Chia Oatmeal','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',158,NULL,2,'foods/05.jpg','published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(6,'Fruit Parfait','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',166,NULL,2,'foods/06.jpg','published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(7,'Marmalade Selection','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',147,NULL,3,'foods/07.jpg','published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(8,'Cheese Platen','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',167,NULL,4,'foods/08.jpg','published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(9,'Avocado Toast','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',179,NULL,5,'foods/09.jpg','published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(10,'Avocado Toast','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',124,NULL,6,'foods/10.jpg','published','2024-02-24 01:24:53','2024-02-24 01:24:53');
/*!40000 ALTER TABLE `ht_foods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_foods_translations`
--

DROP TABLE IF EXISTS `ht_foods_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_foods_translations` (
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ht_foods_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lang_code`,`ht_foods_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_foods_translations`
--

LOCK TABLES `ht_foods_translations` WRITE;
/*!40000 ALTER TABLE `ht_foods_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_foods_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_invoice_items`
--

DROP TABLE IF EXISTS `ht_invoice_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_invoice_items` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int unsigned NOT NULL,
  `sub_total` decimal(15,2) unsigned NOT NULL,
  `tax_amount` decimal(15,2) unsigned NOT NULL DEFAULT '0.00',
  `discount_amount` decimal(15,2) unsigned NOT NULL DEFAULT '0.00',
  `amount` decimal(15,2) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_invoice_items`
--

LOCK TABLES `ht_invoice_items` WRITE;
/*!40000 ALTER TABLE `ht_invoice_items` DISABLE KEYS */;
INSERT INTO `ht_invoice_items` VALUES (1,1,'Luxury Hall Of Fame','',1,140.00,0.00,0.00,140.00,'2024-02-24 01:25:04','2024-02-24 01:25:04'),(2,1,'Breakfast (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:04','2024-02-24 01:25:04'),(3,2,'Family Suite','',1,110.00,0.00,0.00,110.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(4,2,'Sea View (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(5,3,'Family Suite','',1,110.00,0.00,0.00,110.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(6,3,'Car Rental (extra service)','',1,30.00,0.00,0.00,30.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(7,4,'Junior Suite','',1,135.00,0.00,0.00,135.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(8,4,'Laundry (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(9,5,'Pacific Room','',1,196.00,0.00,0.00,196.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(10,5,'Laundry (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(11,6,'Luxury Hall Of Fame','',1,140.00,0.00,0.00,140.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(12,6,'Breakfast (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(13,7,'Family Suite','',1,110.00,0.00,0.00,110.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(14,7,'Satellite TV (extra service)','',1,50.00,0.00,0.00,50.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(15,8,'Junior Suite','',1,135.00,0.00,0.00,135.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(16,8,'Car Rental (extra service)','',1,30.00,0.00,0.00,30.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(17,9,'Junior Suite','',1,135.00,0.00,0.00,135.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(18,9,'Laundry (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(19,10,'President Room','',1,184.00,0.00,0.00,184.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(20,10,'Satellite TV (extra service)','',1,50.00,0.00,0.00,50.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(21,11,'Junior Suite','',1,135.00,0.00,0.00,135.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(22,11,'Car Rental (extra service)','',1,30.00,0.00,0.00,30.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(23,12,'Pacific Room','',1,196.00,0.00,0.00,196.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(24,12,'Satellite TV (extra service)','',1,50.00,0.00,0.00,50.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(25,13,'Relax Suite','',1,114.00,0.00,0.00,114.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(26,13,'Satellite TV (extra service)','',1,50.00,0.00,0.00,50.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(27,14,'Pacific Room','',1,196.00,0.00,0.00,196.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(28,14,'Sea View (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(29,15,'Luxury Suite','',1,117.00,0.00,0.00,117.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(30,15,'Wifi (extra service)','',1,100.00,0.00,0.00,100.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(31,16,'Luxury Suite','',1,117.00,0.00,0.00,117.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(32,16,'Satellite TV (extra service)','',1,50.00,0.00,0.00,50.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(33,17,'Relax Suite','',1,114.00,0.00,0.00,114.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(34,17,'Breakfast (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(35,18,'Relax Suite','',1,114.00,0.00,0.00,114.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(36,18,'Satellite TV (extra service)','',1,50.00,0.00,0.00,50.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(37,19,'Pendora Fame','',1,183.00,0.00,0.00,183.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(38,19,'Sea View (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(39,20,'Luxury Hall Of Fame','',1,140.00,0.00,0.00,140.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(40,20,'Sea View (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(41,21,'President Room','',1,184.00,0.00,0.00,184.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(42,21,'Sea View (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(43,22,'Pacific Room','',1,196.00,0.00,0.00,196.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(44,22,'Breakfast (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(45,23,'Relax Suite','',1,114.00,0.00,0.00,114.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(46,23,'Car Rental (extra service)','',1,30.00,0.00,0.00,30.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(47,24,'Luxury Hall Of Fame','',1,140.00,0.00,0.00,140.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(48,24,'Satellite TV (extra service)','',1,50.00,0.00,0.00,50.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(49,25,'Pendora Fame','',1,183.00,0.00,0.00,183.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(50,25,'Car Rental (extra service)','',1,30.00,0.00,0.00,30.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(51,26,'Pendora Fame','',1,183.00,0.00,0.00,183.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(52,26,'Sea View (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(53,27,'Junior Suite','',1,135.00,0.00,0.00,135.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(54,27,'Sea View (extra service)','',1,10.00,0.00,0.00,10.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(55,28,'Family Suite','',1,110.00,0.00,0.00,110.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(56,28,'Wifi (extra service)','',1,100.00,0.00,0.00,100.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(57,29,'Junior Suite','',1,135.00,0.00,0.00,135.00,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(58,29,'Wifi (extra service)','',1,100.00,0.00,0.00,100.00,'2024-02-24 01:25:05','2024-02-24 01:25:05');
/*!40000 ALTER TABLE `ht_invoice_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_invoices`
--

DROP TABLE IF EXISTS `ht_invoices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_invoices` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint unsigned DEFAULT NULL,
  `customer_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_id` bigint unsigned DEFAULT NULL,
  `reference_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_id` bigint unsigned NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` decimal(15,2) unsigned NOT NULL,
  `tax_amount` decimal(15,2) unsigned NOT NULL DEFAULT '0.00',
  `discount_amount` decimal(15,2) unsigned NOT NULL DEFAULT '0.00',
  `amount` decimal(15,2) unsigned NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `paid_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ht_invoices_code_unique` (`code`),
  KEY `ht_invoices_reference_type_reference_id_index` (`reference_type`,`reference_id`),
  KEY `ht_invoices_payment_id_index` (`payment_id`),
  KEY `ht_invoices_status_index` (`status`),
  KEY `ht_invoices_customer_id_index` (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_invoices`
--

LOCK TABLES `ht_invoices` WRITE;
/*!40000 ALTER TABLE `ht_invoices` DISABLE KEYS */;
INSERT INTO `ht_invoices` VALUES (1,5,'RafaelMarvin','forest94@example.org','307.693.4954','4722 Lauriane Hollow\nWest Dino, NM 93388, Lake Clotildemouth, Lake Flavioland, Guyana, 95795-6474, ','Nemo corporis consequatur assumenda placeat voluptatem exercitationem quo.',1,'Botble\\Hotel\\Models\\Booking',1,'INV-1',140.00,0.00,0.00,140.00,'pending',NULL,'2024-02-24 01:25:04','2024-02-24 01:25:04'),(2,9,'PeteWitting','parker.brendan@example.net','423.661.3975','40180 Katrine Landing Apt. 857\nHeidenreichtown, MD 88575-4714, Port Mariela, Johnathantown, Cook Islands, 81929-4239, ','Aliquam provident itaque est perspiciatis.',2,'Botble\\Hotel\\Models\\Booking',2,'INV-2',110.00,0.00,0.00,110.00,'pending',NULL,'2024-02-24 01:25:04','2024-02-24 01:25:04'),(3,2,'LonnieEmmerich','delfina27@example.net','415.952.5773','835 Champlin Lane Apt. 431\nNew Cordeliafort, KY 87661, Emelieport, Rivermouth, India, 84682-4012, ','Temporibus non aut explicabo.',3,'Botble\\Hotel\\Models\\Booking',3,'INV-3',220.00,0.00,0.00,220.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(4,7,'CletusShields','myrl04@example.org','+15095418977','2507 Lexi Turnpike Suite 884\nMyleneton, PA 86288-2835, New Ricky, New Savanna, Paraguay, 15773, ','Alias minima qui enim rem quas at.',4,'Botble\\Hotel\\Models\\Booking',4,'INV-4',270.00,0.00,0.00,270.00,'pending',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(5,10,'StanleyTillman','silas25@example.net','262-307-9524','2867 Estel Track Suite 454\nSouth Lavinabury, CT 02178, Lake Eloiseland, Coralieberg, United States Minor Outlying Islands, 25005-2126, ','Eius architecto qui enim velit ipsam nisi a.',5,'Botble\\Hotel\\Models\\Booking',5,'INV-5',588.00,0.00,0.00,588.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(6,4,'LindseyO\'Keefe','rudy51@example.com','(541) 863-5795','1185 Metz Heights Apt. 112\nLake Raheembury, MS 84681, Port Kip, Veumburgh, Uzbekistan, 53953-3758, ','Id accusantium temporibus qui beatae.',6,'Botble\\Hotel\\Models\\Booking',6,'INV-6',420.00,0.00,0.00,420.00,'completed','2024-02-24 01:25:05','2024-02-24 01:25:05','2024-02-24 01:25:05'),(7,9,'AgustinaErnser','leannon.nia@example.org','+1 (539) 444-2444','2600 Block Views\nWest Wileymouth, TN 35887, Sonyachester, McCulloughberg, Mongolia, 36985, ','Doloremque nisi sit porro sit dolor.',7,'Botble\\Hotel\\Models\\Booking',7,'INV-7',110.00,0.00,0.00,110.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(8,4,'VictorD\'Amore','davion18@example.net','+16516708854','626 Keaton Cliff Suite 400\nDestinyberg, NM 39860, Greenfelderhaven, Wildermanville, Finland, 74888-0268, ','Magni et minima sapiente assumenda.',8,'Botble\\Hotel\\Models\\Booking',8,'INV-8',135.00,0.00,0.00,135.00,'pending',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(9,9,'ZaneBergnaum','greyson06@example.com','940-720-6179','494 Wiza Lodge\nPort Itzelchester, MA 25333, New Lavernaburgh, Christopmouth, Cote d\'Ivoire, 12101-8476, ','Modi in expedita sapiente asperiores sunt reiciendis et.',9,'Botble\\Hotel\\Models\\Booking',9,'INV-9',270.00,0.00,0.00,270.00,'pending',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(10,11,'MagnusOrn','garret.trantow@example.org','240.402.0674','48359 Maximillia Trail Suite 803\nWest Tyson, NE 92766, Marjoriemouth, Claudmouth, Palau, 88083-2029, ','Veniam modi nihil minima sit.',10,'Botble\\Hotel\\Models\\Booking',10,'INV-10',552.00,0.00,0.00,552.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(11,2,'FreddieRobel','jodie60@example.com','364-821-5855','3378 Blanda Club Suite 658\nNew Rowland, VA 22689, Kshlerinview, South Henry, Bulgaria, 14906, ','Inventore officiis error omnis magnam corrupti perspiciatis explicabo.',11,'Botble\\Hotel\\Models\\Booking',11,'INV-11',405.00,0.00,0.00,405.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(12,3,'SvenGoldner','jwhite@example.net','564.924.1030','21107 Walsh Stravenue Apt. 787\nMuraziktown, PA 16511, South Rosettaborough, North Ellis, Togo, 50692, ','Rem culpa atque ad magnam laborum molestiae.',12,'Botble\\Hotel\\Models\\Booking',12,'INV-12',196.00,0.00,0.00,196.00,'completed','2024-02-24 01:25:05','2024-02-24 01:25:05','2024-02-24 01:25:05'),(13,3,'DarenRunolfsson','violette.wisoky@example.com','+1-817-605-9590','89041 Fritsch Ridge\nWeissnatchester, IN 33718, North Deshaun, North Joanneview, Morocco, 66283, ','Nemo voluptas praesentium excepturi voluptatum dolor ipsa vel nam.',13,'Botble\\Hotel\\Models\\Booking',13,'INV-13',342.00,0.00,0.00,342.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(14,10,'CameronAbbott','johns.juana@example.com','1-610-649-0601','8461 Berge Rapid Apt. 880\nWest Ona, DE 10903-6796, Raustad, Mrazland, Angola, 39226-1117, ','Quaerat est enim sint facere saepe voluptas dignissimos.',14,'Botble\\Hotel\\Models\\Booking',14,'INV-14',392.00,0.00,0.00,392.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(15,4,'HavenLuettgen','ricky.batz@example.com','(910) 812-5613','772 Gleichner Overpass\nFrancomouth, AL 84626, North Amy, North Scot, Guam, 35693, ','Doloribus dicta impedit accusantium quisquam architecto omnis quia.',15,'Botble\\Hotel\\Models\\Booking',15,'INV-15',234.00,0.00,0.00,234.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(16,5,'OsvaldoKing','ferne.hartmann@example.com','1-330-258-9427','225 Huels Port Suite 064\nLake Seamus, MI 25250-3008, Jacobsonchester, Port Kevenport, Lithuania, 97698, ','Voluptas consequatur est expedita velit.',16,'Botble\\Hotel\\Models\\Booking',16,'INV-16',351.00,0.00,0.00,351.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(17,6,'ZackKoepp','eblanda@example.net','765.736.5845','99112 Volkman Plaza Apt. 393\nJoelleville, MN 32101, North Leliahaven, Port Ibrahimbury, Cambodia, 95543, ','Nemo voluptas ipsa sed rerum natus.',17,'Botble\\Hotel\\Models\\Booking',17,'INV-17',342.00,0.00,0.00,342.00,'pending',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(18,4,'KadenDoyle','elenor.casper@example.com','+1.732.621.1487','39261 Amira Ford\nWillport, AR 43897, Sawaynhaven, Lake Russell, Bahrain, 72872, ','Et delectus aut rem nobis cupiditate.',18,'Botble\\Hotel\\Models\\Booking',18,'INV-18',342.00,0.00,0.00,342.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(19,6,'KurtBuckridge','beverly.gibson@example.com','351.753.6231','92988 Cormier Tunnel Apt. 117\nEstrellabury, MN 83375, Mauriciofort, Casandrastad, Norfolk Island, 41626-5548, ','Quam accusamus sint perferendis voluptas voluptas provident tempore.',19,'Botble\\Hotel\\Models\\Booking',19,'INV-19',183.00,0.00,0.00,183.00,'pending',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(20,4,'EvalynKulas','mcorkery@example.net','(623) 688-3454','1770 Jones Ridge\nMosciskiside, AK 81372, North Citlalli, Isaacfort, Peru, 72615-6684, ','Voluptatem pariatur ducimus doloribus ut magnam laudantium a qui.',20,'Botble\\Hotel\\Models\\Booking',20,'INV-20',420.00,0.00,0.00,420.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(21,7,'MozelleLarson','ernser.jesus@example.net','+19415153350','35148 Arianna Divide\nWest Lesley, AK 36414-4629, Casperside, Cristside, Iraq, 42845, ','Recusandae repudiandae dolores perferendis recusandae qui suscipit eum.',21,'Botble\\Hotel\\Models\\Booking',21,'INV-21',184.00,0.00,0.00,184.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(22,10,'EmmanuelleBradtke','wintheiser.jody@example.org','+1-334-418-5287','29165 Metz Village\nReichelshire, LA 83020-6718, North Ahmadborough, Hicklebury, Slovenia, 85989, ','Et nesciunt explicabo vel tempore.',22,'Botble\\Hotel\\Models\\Booking',22,'INV-22',392.00,0.00,0.00,392.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(23,8,'AndyKuhn','destiney73@example.org','443-880-5958','9196 Cullen Shores\nEast Jimmyland, WA 12970-0970, East Skyefort, North Davonteland, Madagascar, 00688, ','Perspiciatis ipsa repellat est.',23,'Botble\\Hotel\\Models\\Booking',23,'INV-23',342.00,0.00,0.00,342.00,'completed','2024-02-24 01:25:05','2024-02-24 01:25:05','2024-02-24 01:25:05'),(24,4,'JovanyHoppe','paucek.taryn@example.net','(229) 407-4166','9875 Kale Forge Suite 413\nReingerview, TX 07257, Pattieside, Elnaburgh, Kiribati, 24711, ','Placeat cum modi sunt fugiat debitis exercitationem reiciendis.',24,'Botble\\Hotel\\Models\\Booking',24,'INV-24',420.00,0.00,0.00,420.00,'pending',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(25,8,'SallyWatsica','powlowski.armando@example.net','743.447.1564','3134 Brady Corners Apt. 439\nLake Marlen, NE 16920-2348, New Yessenia, West Everardoview, Mauritania, 55892, ','Voluptatem et temporibus cumque cum saepe aut.',25,'Botble\\Hotel\\Models\\Booking',25,'INV-25',549.00,0.00,0.00,549.00,'pending',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(26,9,'MaximilianReilly','raynor.dorian@example.org','+17624456768','246 Fay Center Apt. 244\nCliftonmouth, NJ 84077-3367, Mertzfurt, McDermottchester, Cape Verde, 10867, ','Quos eum velit inventore tempora dolorum ratione ducimus.',26,'Botble\\Hotel\\Models\\Booking',26,'INV-26',183.00,0.00,0.00,183.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(27,9,'CyrilWilliamson','paucek.demetris@example.net','+1-534-640-8835','27267 Faustino Shores Suite 550\nEast Rebeca, DC 50693, Port Brielle, Wintheiserbury, British Indian Ocean Territory (Chagos Archipelago), 64777-0142, ','Cum voluptate veritatis in.',27,'Botble\\Hotel\\Models\\Booking',27,'INV-27',270.00,0.00,0.00,270.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(28,4,'DaytonD\'Amore','madelyn.quitzon@example.org','620.287.8523','33071 Green Point\nBrionnamouth, FL 54674, Christophefurt, North Maeshire, South Africa, 38302, ','Doloribus ut aut quasi nam omnis eos.',28,'Botble\\Hotel\\Models\\Booking',28,'INV-28',330.00,0.00,0.00,330.00,'pending',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05'),(29,8,'EudoraBecker','eugene17@example.org','+1 (657) 348-4734','7905 Llewellyn Garden\nRyleyton, NE 91200-3183, Nyaview, New Bentown, Singapore, 88666-8054, ','Magnam occaecati placeat mollitia.',29,'Botble\\Hotel\\Models\\Booking',29,'INV-29',405.00,0.00,0.00,405.00,'canceled',NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05');
/*!40000 ALTER TABLE `ht_invoices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_places`
--

DROP TABLE IF EXISTS `ht_places`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_places` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `distance` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_places`
--

LOCK TABLES `ht_places` WRITE;
/*!40000 ALTER TABLE `ht_places` DISABLE KEYS */;
INSERT INTO `ht_places` VALUES (1,'Duplex Restaurant','1,500m | 21 min. Walk',NULL,'Duchess, the Duchess! Oh! won\'t she be savage if I\'ve been changed in the sea. But they HAVE their tails in their mouths--and they\'re all over their shoulders, that all the unjust things--\' when his eye chanced to fall a long tail, certainly,\' said Alice, very earnestly. \'I\'ve had nothing else to do, and perhaps after all it might belong to one of them didn\'t know that cats COULD grin.\' \'They all can,\' said the Dodo. Then they all crowded round it, panting, and asking, \'But who is Dinah, if I chose,\' the Duchess sang the second verse of the month, and doesn\'t tell what o\'clock it is!\' As she said to herself, \'after such a tiny golden key, and Alice\'s first thought was that she had this fit) An obstacle that came between Him, and ourselves, and it. Don\'t let me help to undo it!\' \'I shall sit here,\' he said, turning to Alice: he had to leave it behind?\' She said the White Rabbit as he spoke, and then she noticed a curious appearance in the last few minutes, and she walked off, leaving.','places/01.jpg','published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(2,'Duplex Restaurant','1,500m | 21 min. Walk',NULL,'Rabbit\'s voice along--\'Catch him, you by the end of his Normans--\" How are you getting on now, my dear?\' it continued, turning to Alice for protection. \'You shan\'t be beheaded!\' \'What for?\' said the Caterpillar. Alice thought this must ever be A secret, kept from all the jurors had a pencil that squeaked. This of course, I meant,\' the King said to herself, \'Why, they\'re only a pack of cards!\' At this the whole court was in the wood,\' continued the Pigeon, raising its voice to a day-school, too,\' said Alice; \'that\'s not at all comfortable, and it set to work, and very nearly getting up and to stand on your head-- Do you think I may as well as she listened, or seemed to be found: all she could guess, she was peering about anxiously among the distant sobs of the trees behind him. \'--or next day, maybe,\' the Footman remarked, \'till tomorrow--\' At this moment the King, looking round the table, half hoping that the reason is--\' here the Mock Turtle. \'Very much indeed,\' said Alice. \'What.','places/02.jpg','published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(3,'Duplex Restaurant','1,500m | 21 min. Walk',NULL,'I\'M a Duchess,\' she said to the puppy; whereupon the puppy began a series of short charges at the Queen, turning purple. \'I won\'t!\' said Alice. \'Off with his knuckles. It was so much into the court, without even looking round. \'I\'ll fetch the executioner myself,\' said the Hatter began, in a trembling voice, \'--and I hadn\'t quite finished my tea when I breathe\"!\' \'It IS the use of this rope--Will the roof off.\' After a minute or two to think this a good deal worse off than before, as the rest of the table, but there were any tears. No, there were three gardeners instantly jumped up, and reduced the answer to shillings and pence. \'Take off your hat,\' the King said to Alice, they all stopped and looked very anxiously into her head. \'If I eat or drink under the sea,\' the Gryphon hastily. \'Go on with the time,\' she said, \'than waste it in a great deal to come down the little door into that lovely garden. I think it would be offended again. \'Mine is a very curious thing, and she at once.','places/03.jpg','published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(4,'Duplex Restaurant','1,500m | 21 min. Walk',NULL,'Alice! It was the White Rabbit was no one to listen to me! When I used to say which), and they walked off together. Alice laughed so much surprised, that for two reasons. First, because I\'m on the top of its mouth, and its great eyes half shut. This seemed to have wondered at this, she looked up, and there was room for YOU, and no room to grow larger again, and that\'s very like a snout than a pig, my dear,\' said Alice, \'because I\'m not particular as to prevent its undoing itself,) she carried it off. \'If everybody minded their own business,\' the Duchess sneezed occasionally; and as for the Dormouse,\' thought Alice; \'but a grin without a grin,\' thought Alice; \'I daresay it\'s a French mouse, come over with fright. \'Oh, I BEG your pardon!\' said the Duchess: \'flamingoes and mustard both bite. And the moral of that dark hall, and wander about among those beds of bright flowers and those cool fountains, but she heard the Rabbit was no time to be otherwise.\"\' \'I think I should like to go.','places/04.jpg','published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(5,'Duplex Restaurant','1,500m | 21 min. Walk',NULL,'Alice more boldly: \'you know you\'re growing too.\' \'Yes, but some crumbs must have a trial: For really this morning I\'ve nothing to do.\" Said the mouse doesn\'t get out.\" Only I don\'t take this child away with me,\' thought Alice, \'and if it makes rather a hard word, I will prosecute YOU.--Come, I\'ll take no denial; We must have a trial: For really this morning I\'ve nothing to what I should understand that better,\' Alice said with a T!\' said the King triumphantly, pointing to the Queen. \'Their heads are gone, if it makes me grow smaller, I can creep under the window, I only wish people knew that: then they both sat silent for a long hookah, and taking not the smallest idea how to set them free, Exactly as we needn\'t try to find that she did not come the same size for ten minutes together!\' \'Can\'t remember WHAT things?\' said the Mouse only shook its head to keep herself from being broken. She hastily put down her anger as well to say which), and they can\'t prove I did: there\'s no harm in.','places/05.jpg','published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(6,'Duplex Restaurant','1,500m | 21 min. Walk',NULL,'Which way?\', holding her hand in hand with Dinah, and saying \"Come up again, dear!\" I shall have to ask any more HERE.\' \'But then,\' thought she, \'what would become of me?\' Luckily for Alice, the little door: but, alas! either the locks were too large, or the key was too dark to see a little timidly: \'but it\'s no use denying it. I suppose you\'ll be telling me next that you weren\'t to talk to.\' \'How are you getting on now, my dear?\' it continued, turning to Alice, flinging the baby at her with large eyes like a snout than a pig, my dear,\' said Alice, \'and if it had been, it suddenly appeared again. \'By-the-bye, what became of the sort. Next came an angry voice--the Rabbit\'s--\'Pat! Pat! Where are you?\' And then a row of lamps hanging from the Queen say only yesterday you deserved to be otherwise.\"\' \'I think you could only hear whispers now and then added them up, and there stood the Queen said--\' \'Get to your little boy, And beat him when he pleases!\' CHORUS. \'Wow! wow! wow!\' \'Here! you.','places/06.jpg','published','2024-02-24 01:24:59','2024-02-24 01:24:59');
/*!40000 ALTER TABLE `ht_places` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_places_translations`
--

DROP TABLE IF EXISTS `ht_places_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_places_translations` (
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ht_places_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `distance` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lang_code`,`ht_places_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_places_translations`
--

LOCK TABLES `ht_places_translations` WRITE;
/*!40000 ALTER TABLE `ht_places_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_places_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_room_categories`
--

DROP TABLE IF EXISTS `ht_room_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_room_categories` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` tinyint NOT NULL DEFAULT '0',
  `is_featured` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_room_categories`
--

LOCK TABLES `ht_room_categories` WRITE;
/*!40000 ALTER TABLE `ht_room_categories` DISABLE KEYS */;
INSERT INTO `ht_room_categories` VALUES (1,'Luxury','published','2024-02-24 01:24:50','2024-02-24 01:24:50',0,1),(2,'Family','published','2024-02-24 01:24:50','2024-02-24 01:24:50',0,1),(3,'Double Bed','published','2024-02-24 01:24:50','2024-02-24 01:24:50',0,1),(4,'Relax','published','2024-02-24 01:24:50','2024-02-24 01:24:50',0,1);
/*!40000 ALTER TABLE `ht_room_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_room_categories_translations`
--

DROP TABLE IF EXISTS `ht_room_categories_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_room_categories_translations` (
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ht_room_categories_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lang_code`,`ht_room_categories_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_room_categories_translations`
--

LOCK TABLES `ht_room_categories_translations` WRITE;
/*!40000 ALTER TABLE `ht_room_categories_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_room_categories_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_room_dates`
--

DROP TABLE IF EXISTS `ht_room_dates`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_room_dates` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `room_id` bigint unsigned DEFAULT NULL,
  `start_date` timestamp NULL DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `value` decimal(15,2) DEFAULT NULL,
  `value_type` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'fixed',
  `max_guests` tinyint DEFAULT NULL,
  `active` tinyint DEFAULT '0',
  `note_to_customer` text COLLATE utf8mb4_unicode_ci,
  `note_to_admin` text COLLATE utf8mb4_unicode_ci,
  `number_of_rooms` smallint DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_room_dates`
--

LOCK TABLES `ht_room_dates` WRITE;
/*!40000 ALTER TABLE `ht_room_dates` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_room_dates` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_room_reviews`
--

DROP TABLE IF EXISTS `ht_room_reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_room_reviews` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint unsigned NOT NULL,
  `room_id` int NOT NULL,
  `star` tinyint NOT NULL,
  `content` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'approved',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_room_reviews`
--

LOCK TABLES `ht_room_reviews` WRITE;
/*!40000 ALTER TABLE `ht_room_reviews` DISABLE KEYS */;
INSERT INTO `ht_room_reviews` VALUES (1,6,5,4,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(2,3,5,5,'A hidden gem! The room was a haven of tranquility, providing a peaceful escape from the bustling city. I appreciated the little touches that made my stay truly special.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(3,1,1,4,'Exceeded all my hopes! The room was not only comfortable but also surprisingly spacious. I loved the attention to cleanliness and the warm, inviting atmosphere.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(4,4,6,5,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(5,5,4,5,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(6,7,1,5,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(7,3,3,4,'A hidden gem! The room was a haven of tranquility, providing a peaceful escape from the bustling city. I appreciated the little touches that made my stay truly special.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(8,4,3,5,'A hidden gem! The room was a haven of tranquility, providing a peaceful escape from the bustling city. I appreciated the little touches that made my stay truly special.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(9,4,7,4,'I couldn’t have asked for a better place to stay. The room’s design was elegant, and the comfort level was off the charts. Staying here added a layer of luxury to my trip.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(10,11,3,4,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(11,2,7,4,'Top-notch accommodations! The room was well-appointed and had all the necessary amenities. The staff was incredibly friendly and made my stay even more enjoyable.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(12,5,5,4,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(13,4,6,4,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(14,4,1,4,'Exceeded all my hopes! The room was not only comfortable but also surprisingly spacious. I loved the attention to cleanliness and the warm, inviting atmosphere.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(15,2,3,4,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(16,6,8,5,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(17,7,6,5,'Five-star experience all the way. The room was meticulously maintained, and the staff was incredibly helpful throughout my stay. I’m already planning my next visit.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(18,8,5,4,'I couldn’t have asked for a better place to stay. The room’s design was elegant, and the comfort level was off the charts. Staying here added a layer of luxury to my trip.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(19,7,8,4,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(20,4,2,4,'I couldn’t have asked for a better place to stay. The room’s design was elegant, and the comfort level was off the charts. Staying here added a layer of luxury to my trip.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(21,2,5,5,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(22,3,7,5,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(23,3,6,5,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(24,3,6,5,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(25,6,8,5,'Top-notch accommodations! The room was well-appointed and had all the necessary amenities. The staff was incredibly friendly and made my stay even more enjoyable.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(26,4,2,4,'Exceeded all my hopes! The room was not only comfortable but also surprisingly spacious. I loved the attention to cleanliness and the warm, inviting atmosphere.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(27,9,5,5,'Top-notch accommodations! The room was well-appointed and had all the necessary amenities. The staff was incredibly friendly and made my stay even more enjoyable.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(28,11,5,5,'Absolutely loved my experience here! The room was not only clean and comfortable but also offered stunning views of the surrounding area. A perfect choice for a relaxing getaway.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(29,4,4,4,'I couldn’t have asked for a better place to stay. The room’s design was elegant, and the comfort level was off the charts. Staying here added a layer of luxury to my trip.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(30,6,2,4,'Top-notch accommodations! The room was well-appointed and had all the necessary amenities. The staff was incredibly friendly and made my stay even more enjoyable.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(31,11,5,5,'Exceeded all my hopes! The room was not only comfortable but also surprisingly spacious. I loved the attention to cleanliness and the warm, inviting atmosphere.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(32,10,3,5,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(33,2,3,5,'Exceeded all my hopes! The room was not only comfortable but also surprisingly spacious. I loved the attention to cleanliness and the warm, inviting atmosphere.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(34,5,5,4,'Top-notch accommodations! The room was well-appointed and had all the necessary amenities. The staff was incredibly friendly and made my stay even more enjoyable.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(35,8,5,4,'Five-star experience all the way. The room was meticulously maintained, and the staff was incredibly helpful throughout my stay. I’m already planning my next visit.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(36,9,2,4,'I couldn’t have asked for a better place to stay. The room’s design was elegant, and the comfort level was off the charts. Staying here added a layer of luxury to my trip.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(37,8,5,5,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(38,1,1,5,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(39,8,7,5,'Absolutely loved my experience here! The room was not only clean and comfortable but also offered stunning views of the surrounding area. A perfect choice for a relaxing getaway.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(40,7,3,4,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(41,3,7,5,'Five-star experience all the way. The room was meticulously maintained, and the staff was incredibly helpful throughout my stay. I’m already planning my next visit.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(42,5,2,4,'Exceeded all my hopes! The room was not only comfortable but also surprisingly spacious. I loved the attention to cleanliness and the warm, inviting atmosphere.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(43,4,8,5,'Top-notch accommodations! The room was well-appointed and had all the necessary amenities. The staff was incredibly friendly and made my stay even more enjoyable.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(44,7,7,4,'I couldn’t have asked for a better place to stay. The room’s design was elegant, and the comfort level was off the charts. Staying here added a layer of luxury to my trip.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(45,11,8,4,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(46,8,4,4,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(47,4,4,4,'I was thoroughly impressed with the attention to detail in the room. Everything from the cozy bed to the modern bathroom exceeded my expectations. Highly recommend!','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(48,6,6,5,'Top-notch accommodations! The room was well-appointed and had all the necessary amenities. The staff was incredibly friendly and made my stay even more enjoyable.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(49,6,3,4,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58'),(50,2,3,4,'An incredible stay! The room was spacious and beautifully decorated. The amenities provided made me feel right at home. I can’t wait to come back.','approved','2024-02-24 01:24:58','2024-02-24 01:24:58');
/*!40000 ALTER TABLE `ht_room_reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_rooms`
--

DROP TABLE IF EXISTS `ht_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_rooms` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `is_featured` tinyint unsigned NOT NULL DEFAULT '0',
  `images` text COLLATE utf8mb4_unicode_ci,
  `price` decimal(15,0) unsigned DEFAULT NULL,
  `currency_id` bigint unsigned DEFAULT NULL,
  `number_of_rooms` int unsigned DEFAULT '0',
  `number_of_beds` int unsigned DEFAULT '0',
  `size` int unsigned DEFAULT '0',
  `max_adults` int DEFAULT '0',
  `max_children` int DEFAULT '0',
  `room_category_id` bigint unsigned DEFAULT NULL,
  `tax_id` bigint unsigned DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_rooms`
--

LOCK TABLES `ht_rooms` WRITE;
/*!40000 ALTER TABLE `ht_rooms` DISABLE KEYS */;
INSERT INTO `ht_rooms` VALUES (1,'Luxury Hall Of Fame','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,0,'[\"rooms\\/01.jpg\",\"rooms\\/02.jpg\",\"rooms\\/03.jpg\",\"rooms\\/04.jpg\",\"rooms\\/05.jpg\",\"rooms\\/06.jpg\"]',140,NULL,5,1,196,4,1,1,1,'published','2024-02-24 01:24:51','2024-02-24 01:24:51',0),(2,'Pendora Fame','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,1,'[\"rooms\\/02.jpg\",\"rooms\\/01.jpg\",\"rooms\\/03.jpg\",\"rooms\\/04.jpg\",\"rooms\\/05.jpg\",\"rooms\\/06.jpg\"]',183,NULL,8,2,153,2,3,1,1,'published','2024-02-24 01:24:51','2024-02-24 01:24:51',0),(3,'Pacific Room','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,1,'[\"rooms\\/03.jpg\",\"rooms\\/02.jpg\",\"rooms\\/01.jpg\",\"rooms\\/04.jpg\",\"rooms\\/05.jpg\",\"rooms\\/06.jpg\"]',196,NULL,4,4,131,4,3,1,1,'published','2024-02-24 01:24:51','2024-02-24 01:24:51',0),(4,'Junior Suite','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,0,'[\"rooms\\/04.jpg\",\"rooms\\/02.jpg\",\"rooms\\/01.jpg\",\"rooms\\/04.jpg\",\"rooms\\/05.jpg\",\"rooms\\/06.jpg\"]',135,NULL,6,3,153,3,1,1,1,'published','2024-02-24 01:24:51','2024-02-24 01:24:51',0),(5,'Family Suite','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,0,'[\"rooms\\/05.jpg\"]',110,NULL,10,4,172,3,3,1,1,'published','2024-02-24 01:24:51','2024-02-24 01:24:51',0),(6,'Relax Suite','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,1,'[\"rooms\\/06.jpg\",\"rooms\\/02.jpg\",\"rooms\\/03.jpg\",\"rooms\\/04.jpg\",\"rooms\\/05.jpg\",\"rooms\\/01.jpg\"]',114,NULL,9,4,169,6,3,3,1,'published','2024-02-24 01:24:51','2024-02-24 01:24:51',0),(7,'Luxury Suite','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,0,'[\"rooms\\/01.jpg\",\"rooms\\/02.jpg\",\"rooms\\/03.jpg\",\"rooms\\/04.jpg\",\"rooms\\/05.jpg\",\"rooms\\/06.jpg\"]',117,NULL,5,4,194,5,2,3,1,'published','2024-02-24 01:24:51','2024-02-24 01:24:51',0),(8,'President Room','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,1,'[\"rooms\\/02.jpg\",\"rooms\\/01.jpg\",\"rooms\\/03.jpg\",\"rooms\\/04.jpg\",\"rooms\\/05.jpg\",\"rooms\\/06.jpg\"]',184,NULL,8,2,162,3,1,1,1,'published','2024-02-24 01:24:51','2024-02-24 01:24:51',0);
/*!40000 ALTER TABLE `ht_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_rooms_amenities`
--

DROP TABLE IF EXISTS `ht_rooms_amenities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_rooms_amenities` (
  `amenity_id` bigint unsigned NOT NULL,
  `room_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`amenity_id`,`room_id`),
  KEY `ht_rooms_amenities_amenity_id_index` (`amenity_id`),
  KEY `ht_rooms_amenities_room_id_index` (`room_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_rooms_amenities`
--

LOCK TABLES `ht_rooms_amenities` WRITE;
/*!40000 ALTER TABLE `ht_rooms_amenities` DISABLE KEYS */;
INSERT INTO `ht_rooms_amenities` VALUES (1,1,NULL,NULL),(1,2,NULL,NULL),(1,3,NULL,NULL),(1,4,NULL,NULL),(1,5,NULL,NULL),(1,6,NULL,NULL),(1,7,NULL,NULL),(1,8,NULL,NULL),(2,1,NULL,NULL),(2,2,NULL,NULL),(2,3,NULL,NULL),(2,4,NULL,NULL),(2,5,NULL,NULL),(2,6,NULL,NULL),(2,7,NULL,NULL),(2,8,NULL,NULL),(3,1,NULL,NULL),(3,2,NULL,NULL),(3,3,NULL,NULL),(3,4,NULL,NULL),(3,5,NULL,NULL),(3,6,NULL,NULL),(3,7,NULL,NULL),(3,8,NULL,NULL),(4,1,NULL,NULL),(4,2,NULL,NULL),(4,3,NULL,NULL),(4,4,NULL,NULL),(4,5,NULL,NULL),(4,6,NULL,NULL),(4,7,NULL,NULL),(4,8,NULL,NULL),(6,1,NULL,NULL),(6,2,NULL,NULL),(6,3,NULL,NULL),(6,4,NULL,NULL),(6,5,NULL,NULL),(6,6,NULL,NULL),(6,7,NULL,NULL),(6,8,NULL,NULL),(7,1,NULL,NULL),(7,2,NULL,NULL),(7,3,NULL,NULL),(7,4,NULL,NULL),(7,5,NULL,NULL),(7,6,NULL,NULL),(7,7,NULL,NULL),(7,8,NULL,NULL),(9,1,NULL,NULL),(9,2,NULL,NULL),(9,3,NULL,NULL),(9,4,NULL,NULL),(9,5,NULL,NULL),(9,6,NULL,NULL),(9,7,NULL,NULL),(9,8,NULL,NULL),(11,1,NULL,NULL),(11,2,NULL,NULL),(11,3,NULL,NULL),(11,4,NULL,NULL),(11,5,NULL,NULL),(11,6,NULL,NULL),(11,7,NULL,NULL),(11,8,NULL,NULL);
/*!40000 ALTER TABLE `ht_rooms_amenities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_rooms_translations`
--

DROP TABLE IF EXISTS `ht_rooms_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_rooms_translations` (
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ht_rooms_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lang_code`,`ht_rooms_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_rooms_translations`
--

LOCK TABLES `ht_rooms_translations` WRITE;
/*!40000 ALTER TABLE `ht_rooms_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_rooms_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_services`
--

DROP TABLE IF EXISTS `ht_services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_services` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `price` decimal(15,0) unsigned DEFAULT NULL,
  `price_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'once',
  `currency_id` bigint unsigned DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_services`
--

LOCK TABLES `ht_services` WRITE;
/*!40000 ALTER TABLE `ht_services` DISABLE KEYS */;
INSERT INTO `ht_services` VALUES (1,'Wifi','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,100,'once',NULL,NULL,'published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(2,'Car Rental','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,30,'once',NULL,NULL,'published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(3,'Satellite TV','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,50,'once',NULL,NULL,'published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(4,'Sea View','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,10,'once',NULL,NULL,'published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(5,'Laundry','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,10,'once',NULL,NULL,'published','2024-02-24 01:24:53','2024-02-24 01:24:53'),(6,'Breakfast','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vel molestie nisl. Duis ac mi leo.',NULL,10,'once',NULL,NULL,'published','2024-02-24 01:24:53','2024-02-24 01:24:53');
/*!40000 ALTER TABLE `ht_services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_services_translations`
--

DROP TABLE IF EXISTS `ht_services_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_services_translations` (
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ht_services_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lang_code`,`ht_services_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_services_translations`
--

LOCK TABLES `ht_services_translations` WRITE;
/*!40000 ALTER TABLE `ht_services_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `ht_services_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ht_taxes`
--

DROP TABLE IF EXISTS `ht_taxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ht_taxes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percentage` double(8,6) DEFAULT NULL,
  `priority` int DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ht_taxes`
--

LOCK TABLES `ht_taxes` WRITE;
/*!40000 ALTER TABLE `ht_taxes` DISABLE KEYS */;
INSERT INTO `ht_taxes` VALUES (1,'VAT',10.000000,1,'published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(2,'None',0.000000,2,'published','2024-02-24 01:24:59','2024-02-24 01:24:59');
/*!40000 ALTER TABLE `ht_taxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jobs`
--

DROP TABLE IF EXISTS `jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `queue` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint unsigned NOT NULL,
  `reserved_at` int unsigned DEFAULT NULL,
  `available_at` int unsigned NOT NULL,
  `created_at` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `jobs_queue_index` (`queue`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jobs`
--

LOCK TABLES `jobs` WRITE;
/*!40000 ALTER TABLE `jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language_meta`
--

DROP TABLE IF EXISTS `language_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `language_meta` (
  `lang_meta_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `lang_meta_code` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_meta_origin` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_id` bigint unsigned NOT NULL,
  `reference_type` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`lang_meta_id`),
  KEY `language_meta_reference_id_index` (`reference_id`),
  KEY `meta_code_index` (`lang_meta_code`),
  KEY `meta_origin_index` (`lang_meta_origin`),
  KEY `meta_reference_type_index` (`reference_type`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language_meta`
--

LOCK TABLES `language_meta` WRITE;
/*!40000 ALTER TABLE `language_meta` DISABLE KEYS */;
INSERT INTO `language_meta` VALUES (1,'en_US','0352338b68e38fa915521d74efcb486c',1,'Botble\\Testimonial\\Models\\Testimonial'),(2,'en_US','321b81f703b1a5188c13c93421e894a6',2,'Botble\\Testimonial\\Models\\Testimonial'),(3,'en_US','a8d60b28162720139fae769a73e37521',3,'Botble\\Testimonial\\Models\\Testimonial'),(4,'en_US','d280246ba14f068e12998d5cad8f9c9b',1,'Botble\\Menu\\Models\\MenuLocation'),(5,'en_US','edd6810f133f814eb8532e22cc1c41f8',1,'Botble\\Menu\\Models\\Menu'),(6,'en_US','072873eefcfa6228a3de8c7261730c9e',2,'Botble\\Menu\\Models\\MenuLocation'),(7,'en_US','b622fcf96e3b2c7f732f65b6d32447a3',2,'Botble\\Menu\\Models\\Menu'),(8,'en_US','181ac2552e28363cd90aaefc034e6259',3,'Botble\\Menu\\Models\\Menu');
/*!40000 ALTER TABLE `language_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
  `lang_id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `lang_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_locale` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_flag` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang_is_default` tinyint unsigned NOT NULL DEFAULT '0',
  `lang_order` int NOT NULL DEFAULT '0',
  `lang_is_rtl` tinyint unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  KEY `lang_locale_index` (`lang_locale`),
  KEY `lang_code_index` (`lang_code`),
  KEY `lang_is_default_index` (`lang_is_default`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
INSERT INTO `languages` VALUES (1,'English','en','en_US','us',1,0,0);
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_files`
--

DROP TABLE IF EXISTS `media_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media_files` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alt` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `folder_id` bigint unsigned NOT NULL DEFAULT '0',
  `mime_type` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `options` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_files_user_id_index` (`user_id`),
  KEY `media_files_index` (`folder_id`,`user_id`,`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_files`
--

LOCK TABLES `media_files` WRITE;
/*!40000 ALTER TABLE `media_files` DISABLE KEYS */;
INSERT INTO `media_files` VALUES (1,0,'01','01',1,'image/jpeg',9425,'news/01.jpg','[]','2024-02-24 01:24:49','2024-02-24 01:24:49',NULL),(2,0,'02','02',1,'image/jpeg',9425,'news/02.jpg','[]','2024-02-24 01:24:49','2024-02-24 01:24:49',NULL),(3,0,'03','03',1,'image/jpeg',9425,'news/03.jpg','[]','2024-02-24 01:24:49','2024-02-24 01:24:49',NULL),(4,0,'04','04',1,'image/jpeg',9425,'news/04.jpg','[]','2024-02-24 01:24:49','2024-02-24 01:24:49',NULL),(5,0,'05','05',1,'image/jpeg',9425,'news/05.jpg','[]','2024-02-24 01:24:50','2024-02-24 01:24:50',NULL),(6,0,'06','06',1,'image/jpeg',9425,'news/06.jpg','[]','2024-02-24 01:24:50','2024-02-24 01:24:50',NULL),(7,0,'01','01',2,'image/jpeg',10738,'rooms/01.jpg','[]','2024-02-24 01:24:50','2024-02-24 01:24:50',NULL),(8,0,'02','02',2,'image/jpeg',10738,'rooms/02.jpg','[]','2024-02-24 01:24:50','2024-02-24 01:24:50',NULL),(9,0,'03','03',2,'image/jpeg',10738,'rooms/03.jpg','[]','2024-02-24 01:24:50','2024-02-24 01:24:50',NULL),(10,0,'04','04',2,'image/jpeg',10738,'rooms/04.jpg','[]','2024-02-24 01:24:50','2024-02-24 01:24:50',NULL),(11,0,'05','05',2,'image/jpeg',10738,'rooms/05.jpg','[]','2024-02-24 01:24:51','2024-02-24 01:24:51',NULL),(12,0,'06','06',2,'image/jpeg',10738,'rooms/06.jpg','[]','2024-02-24 01:24:51','2024-02-24 01:24:51',NULL),(13,0,'01','01',3,'image/jpeg',9803,'foods/01.jpg','[]','2024-02-24 01:24:51','2024-02-24 01:24:51',NULL),(14,0,'02','02',3,'image/jpeg',9803,'foods/02.jpg','[]','2024-02-24 01:24:51','2024-02-24 01:24:51',NULL),(15,0,'03','03',3,'image/jpeg',9803,'foods/03.jpg','[]','2024-02-24 01:24:51','2024-02-24 01:24:51',NULL),(16,0,'04','04',3,'image/jpeg',9803,'foods/04.jpg','[]','2024-02-24 01:24:51','2024-02-24 01:24:51',NULL),(17,0,'05','05',3,'image/jpeg',9803,'foods/05.jpg','[]','2024-02-24 01:24:52','2024-02-24 01:24:52',NULL),(18,0,'06','06',3,'image/jpeg',9803,'foods/06.jpg','[]','2024-02-24 01:24:52','2024-02-24 01:24:52',NULL),(19,0,'07','07',3,'image/jpeg',9803,'foods/07.jpg','[]','2024-02-24 01:24:52','2024-02-24 01:24:52',NULL),(20,0,'08','08',3,'image/jpeg',9803,'foods/08.jpg','[]','2024-02-24 01:24:52','2024-02-24 01:24:52',NULL),(21,0,'09','09',3,'image/jpeg',9803,'foods/09.jpg','[]','2024-02-24 01:24:53','2024-02-24 01:24:53',NULL),(22,0,'10','10',3,'image/jpeg',9803,'foods/10.jpg','[]','2024-02-24 01:24:53','2024-02-24 01:24:53',NULL),(23,0,'1','1',4,'image/jpeg',8581,'customers/1.jpg','[]','2024-02-24 01:24:53','2024-02-24 01:24:53',NULL),(24,0,'10','10',4,'image/jpeg',20004,'customers/10.jpg','[]','2024-02-24 01:24:53','2024-02-24 01:24:53',NULL),(25,0,'2','2',4,'image/jpeg',14257,'customers/2.jpg','[]','2024-02-24 01:24:53','2024-02-24 01:24:53',NULL),(26,0,'3','3',4,'image/jpeg',14702,'customers/3.jpg','[]','2024-02-24 01:24:53','2024-02-24 01:24:53',NULL),(27,0,'4','4',4,'image/jpeg',19699,'customers/4.jpg','[]','2024-02-24 01:24:54','2024-02-24 01:24:54',NULL),(28,0,'5','5',4,'image/jpeg',10260,'customers/5.jpg','[]','2024-02-24 01:24:54','2024-02-24 01:24:54',NULL),(29,0,'6','6',4,'image/jpeg',8476,'customers/6.jpg','[]','2024-02-24 01:24:54','2024-02-24 01:24:54',NULL),(30,0,'7','7',4,'image/jpeg',14388,'customers/7.jpg','[]','2024-02-24 01:24:54','2024-02-24 01:24:54',NULL),(31,0,'8','8',4,'image/jpeg',14340,'customers/8.jpg','[]','2024-02-24 01:24:54','2024-02-24 01:24:54',NULL),(32,0,'9','9',4,'image/jpeg',4396,'customers/9.jpg','[]','2024-02-24 01:24:54','2024-02-24 01:24:54',NULL),(33,0,'01','01',5,'image/jpeg',8820,'places/01.jpg','[]','2024-02-24 01:24:58','2024-02-24 01:24:58',NULL),(34,0,'02','02',5,'image/jpeg',8820,'places/02.jpg','[]','2024-02-24 01:24:58','2024-02-24 01:24:58',NULL),(35,0,'03','03',5,'image/jpeg',8820,'places/03.jpg','[]','2024-02-24 01:24:59','2024-02-24 01:24:59',NULL),(36,0,'04','04',5,'image/jpeg',8820,'places/04.jpg','[]','2024-02-24 01:24:59','2024-02-24 01:24:59',NULL),(37,0,'05','05',5,'image/jpeg',8820,'places/05.jpg','[]','2024-02-24 01:24:59','2024-02-24 01:24:59',NULL),(38,0,'06','06',5,'image/jpeg',8820,'places/06.jpg','[]','2024-02-24 01:24:59','2024-02-24 01:24:59',NULL),(39,0,'01','01',6,'image/png',9803,'testimonials/01.png','[]','2024-02-24 01:24:59','2024-02-24 01:24:59',NULL),(40,0,'02','02',6,'image/png',9803,'testimonials/02.png','[]','2024-02-24 01:25:00','2024-02-24 01:25:00',NULL),(41,0,'03','03',6,'image/png',9803,'testimonials/03.png','[]','2024-02-24 01:25:00','2024-02-24 01:25:00',NULL),(42,0,'01','01',7,'image/jpeg',9803,'galleries/01.jpg','[]','2024-02-24 01:25:00','2024-02-24 01:25:00',NULL),(43,0,'02','02',7,'image/jpeg',9803,'galleries/02.jpg','[]','2024-02-24 01:25:00','2024-02-24 01:25:00',NULL),(44,0,'03','03',7,'image/jpeg',9803,'galleries/03.jpg','[]','2024-02-24 01:25:00','2024-02-24 01:25:00',NULL),(45,0,'04','04',7,'image/jpeg',9803,'galleries/04.jpg','[]','2024-02-24 01:25:00','2024-02-24 01:25:00',NULL),(46,0,'05','05',7,'image/jpeg',9803,'galleries/05.jpg','[]','2024-02-24 01:25:01','2024-02-24 01:25:01',NULL),(47,0,'06','06',7,'image/jpeg',9803,'galleries/06.jpg','[]','2024-02-24 01:25:01','2024-02-24 01:25:01',NULL),(48,0,'07','07',7,'image/jpeg',9803,'galleries/07.jpg','[]','2024-02-24 01:25:01','2024-02-24 01:25:01',NULL),(49,0,'08','08',7,'image/jpeg',9803,'galleries/08.jpg','[]','2024-02-24 01:25:01','2024-02-24 01:25:01',NULL),(50,0,'01','01',8,'image/jpeg',4038,'general/01.jpg','[]','2024-02-24 01:25:02','2024-02-24 01:25:02',NULL),(51,0,'02','02',8,'image/jpeg',2960,'general/02.jpg','[]','2024-02-24 01:25:02','2024-02-24 01:25:02',NULL),(52,0,'03','03',8,'image/jpeg',5120,'general/03.jpg','[]','2024-02-24 01:25:02','2024-02-24 01:25:02',NULL),(53,0,'04','04',8,'image/jpeg',15702,'general/04.jpg','[]','2024-02-24 01:25:03','2024-02-24 01:25:03',NULL),(54,0,'banner-news','banner-news',8,'image/jpeg',8643,'general/banner-news.jpg','[]','2024-02-24 01:25:03','2024-02-24 01:25:03',NULL),(55,0,'bg','bg',8,'image/jpeg',20558,'general/bg.jpg','[]','2024-02-24 01:25:03','2024-02-24 01:25:03',NULL),(56,0,'favicon','favicon',8,'image/png',897,'general/favicon.png','[]','2024-02-24 01:25:03','2024-02-24 01:25:03',NULL),(57,0,'logo-white','logo-white',8,'image/png',2372,'general/logo-white.png','[]','2024-02-24 01:25:03','2024-02-24 01:25:03',NULL),(58,0,'logo','logo',8,'image/png',2383,'general/logo.png','[]','2024-02-24 01:25:03','2024-02-24 01:25:03',NULL),(59,0,'video-background-02','video-background-02',8,'image/jpeg',20022,'general/video-background-02.jpg','[]','2024-02-24 01:25:03','2024-02-24 01:25:03',NULL),(60,0,'video-banner-01','video-banner-01',8,'image/jpeg',11520,'general/video-banner-01.jpg','[]','2024-02-24 01:25:04','2024-02-24 01:25:04',NULL),(61,0,'04','04',9,'image/jpeg',27493,'sliders/04.jpg','[]','2024-02-24 01:25:04','2024-02-24 01:25:04',NULL),(62,0,'05','05',9,'image/jpeg',27493,'sliders/05.jpg','[]','2024-02-24 01:25:04','2024-02-24 01:25:04',NULL);
/*!40000 ALTER TABLE `media_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_folders`
--

DROP TABLE IF EXISTS `media_folders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media_folders` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` bigint unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_folders_user_id_index` (`user_id`),
  KEY `media_folders_index` (`parent_id`,`user_id`,`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_folders`
--

LOCK TABLES `media_folders` WRITE;
/*!40000 ALTER TABLE `media_folders` DISABLE KEYS */;
INSERT INTO `media_folders` VALUES (1,0,'news',NULL,'news',0,'2024-02-24 01:24:49','2024-02-24 01:24:49',NULL),(2,0,'rooms',NULL,'rooms',0,'2024-02-24 01:24:50','2024-02-24 01:24:50',NULL),(3,0,'foods',NULL,'foods',0,'2024-02-24 01:24:51','2024-02-24 01:24:51',NULL),(4,0,'customers',NULL,'customers',0,'2024-02-24 01:24:53','2024-02-24 01:24:53',NULL),(5,0,'places',NULL,'places',0,'2024-02-24 01:24:58','2024-02-24 01:24:58',NULL),(6,0,'testimonials',NULL,'testimonials',0,'2024-02-24 01:24:59','2024-02-24 01:24:59',NULL),(7,0,'galleries',NULL,'galleries',0,'2024-02-24 01:25:00','2024-02-24 01:25:00',NULL),(8,0,'general',NULL,'general',0,'2024-02-24 01:25:02','2024-02-24 01:25:02',NULL),(9,0,'sliders',NULL,'sliders',0,'2024-02-24 01:25:04','2024-02-24 01:25:04',NULL);
/*!40000 ALTER TABLE `media_folders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `media_settings`
--

DROP TABLE IF EXISTS `media_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `media_settings` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `media_id` bigint unsigned DEFAULT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `media_settings`
--

LOCK TABLES `media_settings` WRITE;
/*!40000 ALTER TABLE `media_settings` DISABLE KEYS */;
/*!40000 ALTER TABLE `media_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_locations`
--

DROP TABLE IF EXISTS `menu_locations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu_locations` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` bigint unsigned NOT NULL,
  `location` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_locations_menu_id_created_at_index` (`menu_id`,`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_locations`
--

LOCK TABLES `menu_locations` WRITE;
/*!40000 ALTER TABLE `menu_locations` DISABLE KEYS */;
INSERT INTO `menu_locations` VALUES (1,1,'header-menu','2024-02-24 01:25:02','2024-02-24 01:25:02'),(2,2,'side-menu','2024-02-24 01:25:02','2024-02-24 01:25:02');
/*!40000 ALTER TABLE `menu_locations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_nodes`
--

DROP TABLE IF EXISTS `menu_nodes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menu_nodes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` bigint unsigned NOT NULL,
  `parent_id` bigint unsigned NOT NULL DEFAULT '0',
  `reference_id` bigint unsigned DEFAULT NULL,
  `reference_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon_font` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` tinyint unsigned NOT NULL DEFAULT '0',
  `title` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `css_class` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `target` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `has_child` tinyint unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `menu_nodes_menu_id_index` (`menu_id`),
  KEY `menu_nodes_parent_id_index` (`parent_id`),
  KEY `reference_id` (`reference_id`),
  KEY `reference_type` (`reference_type`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_nodes`
--

LOCK TABLES `menu_nodes` WRITE;
/*!40000 ALTER TABLE `menu_nodes` DISABLE KEYS */;
INSERT INTO `menu_nodes` VALUES (1,1,0,NULL,NULL,'/',NULL,0,'Home',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(2,1,0,NULL,NULL,'/rooms',NULL,0,'Rooms',NULL,'_self',1,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(3,1,2,NULL,NULL,'/rooms/luxury-hall-of-fame',NULL,0,'Luxury Hall Of Fame',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(4,1,2,NULL,NULL,'/rooms/pendora-fame',NULL,0,'Pendora Fame',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(5,1,0,2,'Botble\\Page\\Models\\Page','/news',NULL,0,'News',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(6,1,0,3,'Botble\\Page\\Models\\Page','/contact',NULL,0,'Contact',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(7,2,0,6,'Botble\\Page\\Models\\Page','/about-us',NULL,0,'About Us',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(8,2,0,5,'Botble\\Page\\Models\\Page','/our-gallery',NULL,0,'Our Gallery',NULL,'_self',1,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(9,2,8,NULL,NULL,'/galleries/king-bed',NULL,0,'King Bed',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(10,2,8,NULL,NULL,'/galleries/duplex-restaurant',NULL,0,'Duplex Restaurant',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(11,2,0,4,'Botble\\Page\\Models\\Page','/restaurant',NULL,0,'Restaurant',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(12,2,0,7,'Botble\\Page\\Models\\Page','/places',NULL,0,'Places',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(13,2,0,8,'Botble\\Page\\Models\\Page','/our-offers',NULL,0,'Our Offers',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(14,3,0,NULL,NULL,'#',NULL,0,'Restaurant & Bar',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(15,3,0,NULL,NULL,'#',NULL,0,'Swimming Pool',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(16,3,0,NULL,NULL,'#',NULL,0,'Restaurant',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(17,3,0,NULL,NULL,'#',NULL,0,'Conference Room',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(18,3,0,NULL,NULL,'#',NULL,0,'Cocktail Party Houses',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(19,3,0,NULL,NULL,'#',NULL,0,'Gaming Zone',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(20,3,0,NULL,NULL,'#',NULL,0,'Marriage Party',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(21,3,0,NULL,NULL,'#',NULL,0,'Party Planning',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02'),(22,3,0,NULL,NULL,'#',NULL,0,'Tour Consultancy',NULL,'_self',0,'2024-02-24 01:25:02','2024-02-24 01:25:02');
/*!40000 ALTER TABLE `menu_nodes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `menus` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'Header menu','header-menu','published','2024-02-24 01:25:02','2024-02-24 01:25:02'),(2,'Our pages','our-pages','published','2024-02-24 01:25:02','2024-02-24 01:25:02'),(3,'Services.','services','published','2024-02-24 01:25:02','2024-02-24 01:25:02');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meta_boxes`
--

DROP TABLE IF EXISTS `meta_boxes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `meta_boxes` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_value` text COLLATE utf8mb4_unicode_ci,
  `reference_id` bigint unsigned NOT NULL,
  `reference_type` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `meta_boxes_reference_id_index` (`reference_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meta_boxes`
--

LOCK TABLES `meta_boxes` WRITE;
/*!40000 ALTER TABLE `meta_boxes` DISABLE KEYS */;
/*!40000 ALTER TABLE `meta_boxes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2013_04_09_032329_create_base_tables',1),(2,'2013_04_09_062329_create_revisions_table',1),(3,'2014_10_12_000000_create_users_table',1),(4,'2014_10_12_100000_create_password_reset_tokens_table',1),(5,'2016_06_10_230148_create_acl_tables',1),(6,'2016_06_14_230857_create_menus_table',1),(7,'2016_06_28_221418_create_pages_table',1),(8,'2016_10_05_074239_create_setting_table',1),(9,'2016_11_28_032840_create_dashboard_widget_tables',1),(10,'2016_12_16_084601_create_widgets_table',1),(11,'2017_05_09_070343_create_media_tables',1),(12,'2017_11_03_070450_create_slug_table',1),(13,'2019_01_05_053554_create_jobs_table',1),(14,'2019_08_19_000000_create_failed_jobs_table',1),(15,'2019_12_14_000001_create_personal_access_tokens_table',1),(16,'2021_08_05_134214_fix_social_link_theme_options',1),(17,'2022_04_20_100851_add_index_to_media_table',1),(18,'2022_04_20_101046_add_index_to_menu_table',1),(19,'2022_07_10_034813_move_lang_folder_to_root',1),(20,'2022_08_04_051940_add_missing_column_expires_at',1),(21,'2022_09_01_000001_create_admin_notifications_tables',1),(22,'2022_10_14_024629_drop_column_is_featured',1),(23,'2022_11_18_063357_add_missing_timestamp_in_table_settings',1),(24,'2022_12_02_093615_update_slug_index_columns',1),(25,'2023_01_30_024431_add_alt_to_media_table',1),(26,'2023_02_16_042611_drop_table_password_resets',1),(27,'2023_04_23_005903_add_column_permissions_to_admin_notifications',1),(28,'2023_05_10_075124_drop_column_id_in_role_users_table',1),(29,'2023_08_21_090810_make_page_content_nullable',1),(30,'2023_09_14_021936_update_index_for_slugs_table',1),(31,'2023_12_06_100448_change_random_hash_for_media',1),(32,'2023_12_07_095130_add_color_column_to_media_folders_table',1),(33,'2023_12_17_162208_make_sure_column_color_in_media_folders_nullable',1),(34,'2015_06_29_025744_create_audit_history',2),(35,'2023_11_14_033417_change_request_column_in_table_audit_histories',2),(36,'2015_06_18_033822_create_blog_table',3),(37,'2021_02_16_092633_remove_default_value_for_author_type',3),(38,'2021_12_03_030600_create_blog_translations',3),(39,'2022_04_19_113923_add_index_to_table_posts',3),(40,'2023_08_29_074620_make_column_author_id_nullable',3),(41,'2016_06_17_091537_create_contacts_table',4),(42,'2023_11_10_080225_migrate_contact_blacklist_email_domains_to_core',4),(43,'2016_10_13_150201_create_galleries_table',5),(44,'2021_12_03_082953_create_gallery_translations',5),(45,'2022_04_30_034048_create_gallery_meta_translations_table',5),(46,'2023_08_29_075308_make_column_user_id_nullable',5),(47,'2020_09_02_033611_hotel_create_table',6),(48,'2021_06_25_084734_fix_theme_options',6),(49,'2021_08_18_011425_add_column_order_into_rooms',6),(50,'2021_08_25_153801_update_table_ht_room_categories',6),(51,'2021_08_29_031421_add_translations_tables_for_hotel',6),(52,'2023_04_09_083713_update_hotel_customers_table',6),(53,'2023_04_17_033111_add_booking_number_of_guests',6),(54,'2023_08_11_090349_add_column_password_customers_table',6),(55,'2023_08_14_090449_create_reset_password_table',6),(56,'2023_08_16_063152_update_ht_booking_room_table',6),(57,'2023_08_18_022454_add_new_field_to_ht_customers_table',6),(58,'2023_08_23_022361_create_ht_invoices_table',6),(59,'2023_08_23_041912_create_hotel_review_table',6),(60,'2023_08_23_443543_add_sub_total_to_booking_table',6),(61,'2023_08_23_904382_update_field_customer_id_to_invoice_table',6),(62,'2023_08_24_534892_add_fields_to_invoice_table',6),(63,'2023_08_24_745332_add_field_description_to_invoice_table',6),(64,'2023_08_25_061510_add_adjust_type_and_amount_column',6),(65,'2023_09_05_083354_create_ht_coupons_table',6),(66,'2023_09_06_062315_add_coupon_columns_to_booking_table',6),(67,'2023_10_18_024658_add_price_type_column_to_services_table',6),(68,'2023_10_24_014726_drop_unique_in_room_name',6),(69,'2016_10_03_032336_create_languages_table',7),(70,'2023_09_14_022423_add_index_for_language_table',7),(71,'2021_10_25_021023_fix-priority-load-for-language-advanced',8),(72,'2021_12_03_075608_create_page_translations',8),(73,'2023_07_06_011444_create_slug_translations_table',8),(74,'2017_10_24_154832_create_newsletter_table',9),(75,'2017_05_18_080441_create_payment_tables',10),(76,'2021_03_27_144913_add_customer_type_into_table_payments',10),(77,'2021_05_24_034720_make_column_currency_nullable',10),(78,'2021_08_09_161302_add_metadata_column_to_payments_table',10),(79,'2021_10_19_020859_update_metadata_field',10),(80,'2022_06_28_151901_activate_paypal_stripe_plugin',10),(81,'2022_07_07_153354_update_charge_id_in_table_payments',10),(82,'2018_07_09_214610_create_testimonial_table',11),(83,'2021_12_03_083642_create_testimonials_translations',11),(84,'2016_10_07_193005_create_translations_table',12),(85,'2023_12_12_105220_drop_translations_table',12);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsletters`
--

DROP TABLE IF EXISTS `newsletters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `newsletters` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'subscribed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsletters`
--

LOCK TABLES `newsletters` WRITE;
/*!40000 ALTER TABLE `newsletters` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsletters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pages` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` bigint unsigned DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `template` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pages_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,'Homepage','<div>[home-banner][/home-banner]</div><div>[check-availability-form][/check-availability-form]</div><div>[hotel-about title=\"since 1994\" subtitle=\"Situated In Prime Position At The Foot Of The Slopes Of Courchevel Moriond.\" block_icon_1=\"flaticon-coffee\" block_text_1=\"Breakfast\" block_link_1=\"#\" block_icon_2=\"flaticon-air-freight\" block_text_2=\"Airport Pickup\" block_link_2=\"#\" block_icon_3=\"flaticon-marker\" block_text_3=\"City Guide\" block_link_3=\"#\" block_icon_4=\"flaticon-barbecue\" block_text_4=\"BBQ Party\" block_link_4=\"#\" block_icon_5=\"flaticon-hotel\" block_text_5=\"Luxury Room\" block_link_5=\"#\"][/hotel-about]</div><div>[room-categories title=\"Room Type\" subtitle=\"Inspired Loading\" background_image=\"general/bg.jpg\"][/room-categories]</div><div>[hotel-featured-features title=\"The Thin Escape\" subtitle=\"Miranda has everything for your trip & every single things.\" button_text=\"Take a tour\" button_url=\"/rooms\"][/hotel-featured-features]</div><div>[rooms][/rooms]</div><div>[video-introduction title=\"Take a tour\" subtitle=\"Discover Our Underground.\" content=\"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.\" background_image=\"general/video-background-02.jpg\" video_image=\"general/video-banner-01.jpg\" video_url=\"https://www.youtube.com/watch?v=EEJFMdfraVY\" button_text=\"Book Now\" button_url=\"/rooms\"][/video-introduction]</div><div>[testimonial title=\"testimonials\" subtitle=\"Client Feedback\"][/testimonial]</div><div>[rooms-introduction title=\"Our rooms\" subtitle=\"Each of our nine lovely double guest rooms feature a private bath, wi-fi, cable television and include full breakfast.\" background_image=\"general/bg.jpg\" first_image=\"general/01.jpg\" second_image=\"general/02.jpg\" third_image=\"general/03.jpg\" button_text=\"Take a tour\" button_url=\"/rooms\"][/rooms-introduction]</div><div>[featured-news title=\"Blog\" subtitle=\"News Feeds\"][/featured-news]</div>',1,NULL,'homepage',NULL,'published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(2,'News','<p>--</p>',1,NULL,'default',NULL,'published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(3,'Contact','<div>[contact-info][/contact-info]</div><div>[google-map]19/A, Cirikon City hall Tower New York, NYC[/google-map]</div><div>[contact-form][/contact-form]</div>',1,NULL,'no-sidebar',NULL,'published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(4,'Restaurant','<div>[food-types][/food-types]</div><div>[foods title=\"Restaurant\" subtitle=\"Trending Menu\"][/foods]</div>',1,NULL,'no-sidebar',NULL,'published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(5,'Our Gallery','<div>[all-galleries title=\"Gallery\" subtitle=\"Our Rooms\"][/all-galleries]</div>',1,NULL,'no-sidebar',NULL,'published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(6,'About us','<div>[youtube-video url=\"https://www.youtube.com/watch?v=EEJFMdfraVY\" background_image=\"general/04.jpg\"][/youtube-video]</div><p>Hello. Our hotel has been present for over 20 years. We make the best or all our customers. Hello. Our hotel has been present for over 20 years. We make the best or all our customers. Hello. Our hotel has been present for over 20 years. We make the best or all our customers.</p><div>[hotel-core-features title=\"Facilities\" subtitle=\"Core Features\"][/hotel-core-features]</div><div>[featured-news title=\"Blog\" subtitle=\"News Feeds\"][/featured-news]</div>',1,NULL,'no-sidebar',NULL,'published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(7,'Places','<div>[places][/places]</div>',1,NULL,'no-sidebar',NULL,'published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(8,'Our Offers','<div>[our-offers][/our-offers]</div>',1,NULL,'no-sidebar',NULL,'published','2024-02-24 01:24:59','2024-02-24 01:24:59'),(9,'Cookie Policy','<h3>EU Cookie Consent</h3><p>To use this website we are using Cookies and collecting some data. To be compliant with the EU GDPR we give you to choose if you allow us to use certain Cookies and to collect some Data.</p><h4>Essential Data</h4><p>The Essential Data is needed to run the Site you are visiting technically. You can not deactivate them.</p><p>- Session Cookie: PHP uses a Cookie to identify user sessions. Without this Cookie the Website is not working.</p><p>- XSRF-Token Cookie: Laravel automatically generates a CSRF \"token\" for each active user session managed by the application. This token is used to verify that the authenticated user is the one actually making the requests to the application.</p>',1,NULL,'default',NULL,'published','2024-02-24 01:24:59','2024-02-24 01:24:59');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages_translations`
--

DROP TABLE IF EXISTS `pages_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pages_translations` (
  `lang_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pages_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lang_code`,`pages_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages_translations`
--

LOCK TABLES `pages_translations` WRITE;
/*!40000 ALTER TABLE `pages_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `pages_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_tokens`
--

DROP TABLE IF EXISTS `password_reset_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_reset_tokens` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset_tokens`
--

LOCK TABLES `password_reset_tokens` WRITE;
/*!40000 ALTER TABLE `password_reset_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_reset_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payments`
--

DROP TABLE IF EXISTS `payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payments` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `currency` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint unsigned NOT NULL DEFAULT '0',
  `charge_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_channel` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(15,2) unsigned NOT NULL,
  `order_id` bigint unsigned DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT 'pending',
  `payment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'confirm',
  `customer_id` bigint unsigned DEFAULT NULL,
  `refunded_amount` decimal(15,2) unsigned DEFAULT NULL,
  `refund_note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `customer_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `metadata` mediumtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payments`
--

LOCK TABLES `payments` WRITE;
/*!40000 ALTER TABLE `payments` DISABLE KEYS */;
INSERT INTO `payments` VALUES (1,'USD',5,'QLELl75ZLNkVMUHpThVs','stripe',NULL,140.00,1,'pending','direct',5,NULL,NULL,'2024-02-24 01:25:04','2024-02-24 01:25:04','Botble\\Hotel\\Models\\Customer',NULL),(2,'USD',9,'wPX5CXxzfXkH3TbsGCOf','paypal',NULL,110.00,2,'pending','direct',9,NULL,NULL,'2024-02-24 01:25:04','2024-02-24 01:25:04','Botble\\Hotel\\Models\\Customer',NULL),(3,'USD',2,'MrxK4sRkQSSdy1GPJnKD','stripe',NULL,220.00,3,'fraud','direct',2,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(4,'USD',7,'tlYFw5QkvEDUwiD8IxfH','paystack',NULL,270.00,4,'pending','direct',7,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(5,'USD',10,'xInc4P3qmHPCUaG76Kyb','razorpay',NULL,588.00,5,'refunding','direct',10,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(6,'USD',4,'hX9ff7HvjuZ1wHVUBkdS','stripe',NULL,420.00,6,'completed','direct',4,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(7,'USD',9,'haF83tciaspGNIYM0HKl','cod',NULL,110.00,7,'failed','direct',9,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(8,'USD',4,'vhTKC2XGlgVpL4lucSxT','bank_transfer',NULL,135.00,8,'pending','direct',4,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(9,'USD',9,'2onWa0UZmLVXKSC8wU6Y','cod',NULL,270.00,9,'pending','direct',9,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(10,'USD',11,'aP4rFGFQebjhVM7b7emv','bank_transfer',NULL,552.00,10,'fraud','direct',11,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(11,'USD',2,'hzJhltSF1lfylQDevipJ','sslcommerz',NULL,405.00,11,'fraud','direct',2,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(12,'USD',3,'S8dFPInd260rZ0g5ALKy','razorpay',NULL,196.00,12,'completed','direct',3,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(13,'USD',3,'A21Rs3i8VzXjF1VHOlWd','paypal',NULL,342.00,13,'failed','direct',3,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(14,'USD',10,'stJrKw8mZvjN7kBcI1IU','cod',NULL,392.00,14,'fraud','direct',10,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(15,'USD',4,'kJUVGTRpd6X97L2J9XTA','cod',NULL,234.00,15,'failed','direct',4,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(16,'USD',5,'059gvhzl1EMWtF9h7i6a','cod',NULL,351.00,16,'fraud','direct',5,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(17,'USD',6,'Q22jceB4jsiHssOEv8DH','cod',NULL,342.00,17,'pending','direct',6,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(18,'USD',4,'RoVuMGzeEpeMLjQcb5pj','stripe',NULL,342.00,18,'failed','direct',4,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(19,'USD',6,'WTYlxFsDhUhltUH6ptue','stripe',NULL,183.00,19,'pending','direct',6,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(20,'USD',4,'GJzsyH46Edd6qUiIbc74','sslcommerz',NULL,420.00,20,'failed','direct',4,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(21,'USD',7,'n793Q4SfXnZ8bFODaqt4','paypal',NULL,184.00,21,'refunding','direct',7,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(22,'USD',10,'GM6bevYuvD4zoZnp8S6I','cod',NULL,392.00,22,'failed','direct',10,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(23,'USD',8,'dq723yBCxVoHm42aSfYG','razorpay',NULL,342.00,23,'completed','direct',8,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(24,'USD',4,'iq1wHZcTnvShzorxCVTx','paystack',NULL,420.00,24,'pending','direct',4,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(25,'USD',8,'DkZJutIIJWwsLrrtaxWS','cod',NULL,549.00,25,'pending','direct',8,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(26,'USD',9,'gwxkRtsnkhiZKCKeMDEI','sslcommerz',NULL,183.00,26,'refunded','direct',9,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(27,'USD',9,'I8sRdNhlQ5Vw8BFO0JCL','bank_transfer',NULL,270.00,27,'failed','direct',9,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(28,'USD',4,'hoUdS2EZDsHOWotPegNL','bank_transfer',NULL,330.00,28,'pending','direct',4,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL),(29,'USD',8,'6acPWuh4j7T1IMRzdRRz','stripe',NULL,405.00,29,'failed','direct',8,NULL,NULL,'2024-02-24 01:25:05','2024-02-24 01:25:05','Botble\\Hotel\\Models\\Customer',NULL);
/*!40000 ALTER TABLE `payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_categories`
--

DROP TABLE IF EXISTS `post_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post_categories` (
  `category_id` bigint unsigned NOT NULL,
  `post_id` bigint unsigned NOT NULL,
  KEY `post_categories_category_id_index` (`category_id`),
  KEY `post_categories_post_id_index` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_categories`
--

LOCK TABLES `post_categories` WRITE;
/*!40000 ALTER TABLE `post_categories` DISABLE KEYS */;
INSERT INTO `post_categories` VALUES (2,1),(4,1),(2,2),(3,2),(1,3),(4,3),(2,4),(4,4),(1,5),(3,5),(2,6),(3,6);
/*!40000 ALTER TABLE `post_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_tags`
--

DROP TABLE IF EXISTS `post_tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `post_tags` (
  `tag_id` bigint unsigned NOT NULL,
  `post_id` bigint unsigned NOT NULL,
  KEY `post_tags_tag_id_index` (`tag_id`),
  KEY `post_tags_post_id_index` (`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_tags`
--

LOCK TABLES `post_tags` WRITE;
/*!40000 ALTER TABLE `post_tags` DISABLE KEYS */;
INSERT INTO `post_tags` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(1,2),(2,2),(3,2),(4,2),(5,2),(1,3),(2,3),(3,3),(4,3),(5,3),(1,4),(2,4),(3,4),(4,4),(5,4),(1,5),(2,5),(3,5),(4,5),(5,5),(1,6),(2,6),(3,6),(4,6),(5,6);
/*!40000 ALTER TABLE `post_tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `author_id` bigint unsigned DEFAULT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `is_featured` tinyint unsigned NOT NULL DEFAULT '0',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `views` int unsigned NOT NULL DEFAULT '0',
  `format_type` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `posts_status_index` (`status`),
  KEY `posts_author_id_index` (`author_id`),
  KEY `posts_author_type_index` (`author_type`),
  KEY `posts_created_at_index` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'Each of our 8 double rooms has its own distinct.','You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.','<p>I have seen many people underestimating the power of their wallets. To them, they are just a functional item they use to carry. As a result, they often end up with the wallets which are not really suitable for them.</p>\n\n<p>You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.</p>\n\n<p style=\"text-align:center\"><img alt=\"f4\" src=\"/storage/news/04.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<p><strong><em>For all of the reason above, here are 7 expert tips to help you pick up the right men&rsquo;s wallet for you:</em></strong></p>\n\n<h4><strong>Number 1: Choose A Neat Wallet</strong></h4>\n\n<p>The wallet is an essential accessory that you should go simple. Simplicity is the best in this case. A simple and neat wallet with the plain color and even&nbsp;<strong>minimalist style</strong>&nbsp;is versatile. It can be used for both formal and casual events. In addition, that wallet will go well with most of the clothes in your wardrobe.</p>\n\n<p>Keep in mind that a wallet will tell other people about your personality and your fashion sense as much as other clothes you put on. Hence, don&rsquo;t go cheesy on your wallet or else people will think that you have a funny and particular style.</p>\n\n<p style=\"text-align:center\"><img alt=\"f5\" src=\"/storage/news/05.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n<hr />\n<h4><strong>Number 2: Choose The Right Size For Your Wallet</strong></h4>\n\n<p>You should avoid having an over-sized wallet. Don&rsquo;t think that you need to buy a big wallet because you have a lot to carry with you. In addition, a fat wallet is very ugly. It will make it harder for you to slide the wallet into your trousers&rsquo; pocket. In addition, it will create a bulge and ruin your look.</p>\n\n<p>Before you go on to buy a new wallet, clean out your wallet and place all of the items from your wallet on a table. Throw away things that you would never need any more such as the old bills or the expired gift cards. Remember to check your wallet on a frequent basis to get rid of all of the old stuff that you don&rsquo;t need anymore.</p>\n\n<p style=\"text-align:center\"><img alt=\"f1\" src=\"/storage/news/06.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<hr />\n<h4><strong>Number 3: Don&rsquo;t Limit Your Options Of Materials</strong></h4>\n\n<p>The types and designs of wallets are not the only things that you should consider when you go out searching for your best wallet. You have more than 1 option of material rather than leather to choose from as well.</p>\n\n<p>You can experiment with other available options such as cotton, polyester and canvas. They all have their own pros and cons. As a result, they will be suitable for different needs and requirements. You should think about them all in order to choose the material which you would like the most.</p>\n\n<p>&nbsp;</p>\n','published',1,'Botble\\ACL\\Models\\User',1,'news/01.jpg',1697,NULL,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(2,'Essential Qualities of Highly Successful Music','You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.','<p>I have seen many people underestimating the power of their wallets. To them, they are just a functional item they use to carry. As a result, they often end up with the wallets which are not really suitable for them.</p>\n\n<p>You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.</p>\n\n<p style=\"text-align:center\"><img alt=\"f4\" src=\"/storage/news/04.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<p><strong><em>For all of the reason above, here are 7 expert tips to help you pick up the right men&rsquo;s wallet for you:</em></strong></p>\n\n<h4><strong>Number 1: Choose A Neat Wallet</strong></h4>\n\n<p>The wallet is an essential accessory that you should go simple. Simplicity is the best in this case. A simple and neat wallet with the plain color and even&nbsp;<strong>minimalist style</strong>&nbsp;is versatile. It can be used for both formal and casual events. In addition, that wallet will go well with most of the clothes in your wardrobe.</p>\n\n<p>Keep in mind that a wallet will tell other people about your personality and your fashion sense as much as other clothes you put on. Hence, don&rsquo;t go cheesy on your wallet or else people will think that you have a funny and particular style.</p>\n\n<p style=\"text-align:center\"><img alt=\"f5\" src=\"/storage/news/05.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n<hr />\n<h4><strong>Number 2: Choose The Right Size For Your Wallet</strong></h4>\n\n<p>You should avoid having an over-sized wallet. Don&rsquo;t think that you need to buy a big wallet because you have a lot to carry with you. In addition, a fat wallet is very ugly. It will make it harder for you to slide the wallet into your trousers&rsquo; pocket. In addition, it will create a bulge and ruin your look.</p>\n\n<p>Before you go on to buy a new wallet, clean out your wallet and place all of the items from your wallet on a table. Throw away things that you would never need any more such as the old bills or the expired gift cards. Remember to check your wallet on a frequent basis to get rid of all of the old stuff that you don&rsquo;t need anymore.</p>\n\n<p style=\"text-align:center\"><img alt=\"f1\" src=\"/storage/news/06.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<hr />\n<h4><strong>Number 3: Don&rsquo;t Limit Your Options Of Materials</strong></h4>\n\n<p>The types and designs of wallets are not the only things that you should consider when you go out searching for your best wallet. You have more than 1 option of material rather than leather to choose from as well.</p>\n\n<p>You can experiment with other available options such as cotton, polyester and canvas. They all have their own pros and cons. As a result, they will be suitable for different needs and requirements. You should think about them all in order to choose the material which you would like the most.</p>\n\n<p>&nbsp;</p>\n','published',1,'Botble\\ACL\\Models\\User',1,'news/02.jpg',1333,NULL,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(3,'9 Things I Love About Shaving My Head','You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.','<p>I have seen many people underestimating the power of their wallets. To them, they are just a functional item they use to carry. As a result, they often end up with the wallets which are not really suitable for them.</p>\n\n<p>You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.</p>\n\n<p style=\"text-align:center\"><img alt=\"f4\" src=\"/storage/news/04.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<p><strong><em>For all of the reason above, here are 7 expert tips to help you pick up the right men&rsquo;s wallet for you:</em></strong></p>\n\n<h4><strong>Number 1: Choose A Neat Wallet</strong></h4>\n\n<p>The wallet is an essential accessory that you should go simple. Simplicity is the best in this case. A simple and neat wallet with the plain color and even&nbsp;<strong>minimalist style</strong>&nbsp;is versatile. It can be used for both formal and casual events. In addition, that wallet will go well with most of the clothes in your wardrobe.</p>\n\n<p>Keep in mind that a wallet will tell other people about your personality and your fashion sense as much as other clothes you put on. Hence, don&rsquo;t go cheesy on your wallet or else people will think that you have a funny and particular style.</p>\n\n<p style=\"text-align:center\"><img alt=\"f5\" src=\"/storage/news/05.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n<hr />\n<h4><strong>Number 2: Choose The Right Size For Your Wallet</strong></h4>\n\n<p>You should avoid having an over-sized wallet. Don&rsquo;t think that you need to buy a big wallet because you have a lot to carry with you. In addition, a fat wallet is very ugly. It will make it harder for you to slide the wallet into your trousers&rsquo; pocket. In addition, it will create a bulge and ruin your look.</p>\n\n<p>Before you go on to buy a new wallet, clean out your wallet and place all of the items from your wallet on a table. Throw away things that you would never need any more such as the old bills or the expired gift cards. Remember to check your wallet on a frequent basis to get rid of all of the old stuff that you don&rsquo;t need anymore.</p>\n\n<p style=\"text-align:center\"><img alt=\"f1\" src=\"/storage/news/06.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<hr />\n<h4><strong>Number 3: Don&rsquo;t Limit Your Options Of Materials</strong></h4>\n\n<p>The types and designs of wallets are not the only things that you should consider when you go out searching for your best wallet. You have more than 1 option of material rather than leather to choose from as well.</p>\n\n<p>You can experiment with other available options such as cotton, polyester and canvas. They all have their own pros and cons. As a result, they will be suitable for different needs and requirements. You should think about them all in order to choose the material which you would like the most.</p>\n\n<p>&nbsp;</p>\n','published',1,'Botble\\ACL\\Models\\User',1,'news/03.jpg',194,NULL,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(4,'Why Teamwork Really Makes The Dream Work','You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.','<p>I have seen many people underestimating the power of their wallets. To them, they are just a functional item they use to carry. As a result, they often end up with the wallets which are not really suitable for them.</p>\n\n<p>You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.</p>\n\n<p style=\"text-align:center\"><img alt=\"f4\" src=\"/storage/news/04.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<p><strong><em>For all of the reason above, here are 7 expert tips to help you pick up the right men&rsquo;s wallet for you:</em></strong></p>\n\n<h4><strong>Number 1: Choose A Neat Wallet</strong></h4>\n\n<p>The wallet is an essential accessory that you should go simple. Simplicity is the best in this case. A simple and neat wallet with the plain color and even&nbsp;<strong>minimalist style</strong>&nbsp;is versatile. It can be used for both formal and casual events. In addition, that wallet will go well with most of the clothes in your wardrobe.</p>\n\n<p>Keep in mind that a wallet will tell other people about your personality and your fashion sense as much as other clothes you put on. Hence, don&rsquo;t go cheesy on your wallet or else people will think that you have a funny and particular style.</p>\n\n<p style=\"text-align:center\"><img alt=\"f5\" src=\"/storage/news/05.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n<hr />\n<h4><strong>Number 2: Choose The Right Size For Your Wallet</strong></h4>\n\n<p>You should avoid having an over-sized wallet. Don&rsquo;t think that you need to buy a big wallet because you have a lot to carry with you. In addition, a fat wallet is very ugly. It will make it harder for you to slide the wallet into your trousers&rsquo; pocket. In addition, it will create a bulge and ruin your look.</p>\n\n<p>Before you go on to buy a new wallet, clean out your wallet and place all of the items from your wallet on a table. Throw away things that you would never need any more such as the old bills or the expired gift cards. Remember to check your wallet on a frequent basis to get rid of all of the old stuff that you don&rsquo;t need anymore.</p>\n\n<p style=\"text-align:center\"><img alt=\"f1\" src=\"/storage/news/06.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<hr />\n<h4><strong>Number 3: Don&rsquo;t Limit Your Options Of Materials</strong></h4>\n\n<p>The types and designs of wallets are not the only things that you should consider when you go out searching for your best wallet. You have more than 1 option of material rather than leather to choose from as well.</p>\n\n<p>You can experiment with other available options such as cotton, polyester and canvas. They all have their own pros and cons. As a result, they will be suitable for different needs and requirements. You should think about them all in order to choose the material which you would like the most.</p>\n\n<p>&nbsp;</p>\n','published',1,'Botble\\ACL\\Models\\User',1,'news/04.jpg',1527,NULL,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(5,'The World Caters to Average People','You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.','<p>I have seen many people underestimating the power of their wallets. To them, they are just a functional item they use to carry. As a result, they often end up with the wallets which are not really suitable for them.</p>\n\n<p>You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.</p>\n\n<p style=\"text-align:center\"><img alt=\"f4\" src=\"/storage/news/04.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<p><strong><em>For all of the reason above, here are 7 expert tips to help you pick up the right men&rsquo;s wallet for you:</em></strong></p>\n\n<h4><strong>Number 1: Choose A Neat Wallet</strong></h4>\n\n<p>The wallet is an essential accessory that you should go simple. Simplicity is the best in this case. A simple and neat wallet with the plain color and even&nbsp;<strong>minimalist style</strong>&nbsp;is versatile. It can be used for both formal and casual events. In addition, that wallet will go well with most of the clothes in your wardrobe.</p>\n\n<p>Keep in mind that a wallet will tell other people about your personality and your fashion sense as much as other clothes you put on. Hence, don&rsquo;t go cheesy on your wallet or else people will think that you have a funny and particular style.</p>\n\n<p style=\"text-align:center\"><img alt=\"f5\" src=\"/storage/news/05.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n<hr />\n<h4><strong>Number 2: Choose The Right Size For Your Wallet</strong></h4>\n\n<p>You should avoid having an over-sized wallet. Don&rsquo;t think that you need to buy a big wallet because you have a lot to carry with you. In addition, a fat wallet is very ugly. It will make it harder for you to slide the wallet into your trousers&rsquo; pocket. In addition, it will create a bulge and ruin your look.</p>\n\n<p>Before you go on to buy a new wallet, clean out your wallet and place all of the items from your wallet on a table. Throw away things that you would never need any more such as the old bills or the expired gift cards. Remember to check your wallet on a frequent basis to get rid of all of the old stuff that you don&rsquo;t need anymore.</p>\n\n<p style=\"text-align:center\"><img alt=\"f1\" src=\"/storage/news/06.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<hr />\n<h4><strong>Number 3: Don&rsquo;t Limit Your Options Of Materials</strong></h4>\n\n<p>The types and designs of wallets are not the only things that you should consider when you go out searching for your best wallet. You have more than 1 option of material rather than leather to choose from as well.</p>\n\n<p>You can experiment with other available options such as cotton, polyester and canvas. They all have their own pros and cons. As a result, they will be suitable for different needs and requirements. You should think about them all in order to choose the material which you would like the most.</p>\n\n<p>&nbsp;</p>\n','published',1,'Botble\\ACL\\Models\\User',1,'news/05.jpg',1453,NULL,'2024-02-24 01:24:50','2024-02-24 01:24:50'),(6,'The litigants on the screen are not actors','You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.','<p>I have seen many people underestimating the power of their wallets. To them, they are just a functional item they use to carry. As a result, they often end up with the wallets which are not really suitable for them.</p>\n\n<p>You should pay more attention when you choose your wallets. There are a lot of them on the market with the different designs and styles. When you choose carefully, you would be able to buy a wallet that is catered to your needs. Not to mention that it will help to enhance your style significantly.</p>\n\n<p style=\"text-align:center\"><img alt=\"f4\" src=\"/storage/news/04.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<p><strong><em>For all of the reason above, here are 7 expert tips to help you pick up the right men&rsquo;s wallet for you:</em></strong></p>\n\n<h4><strong>Number 1: Choose A Neat Wallet</strong></h4>\n\n<p>The wallet is an essential accessory that you should go simple. Simplicity is the best in this case. A simple and neat wallet with the plain color and even&nbsp;<strong>minimalist style</strong>&nbsp;is versatile. It can be used for both formal and casual events. In addition, that wallet will go well with most of the clothes in your wardrobe.</p>\n\n<p>Keep in mind that a wallet will tell other people about your personality and your fashion sense as much as other clothes you put on. Hence, don&rsquo;t go cheesy on your wallet or else people will think that you have a funny and particular style.</p>\n\n<p style=\"text-align:center\"><img alt=\"f5\" src=\"/storage/news/05.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n<hr />\n<h4><strong>Number 2: Choose The Right Size For Your Wallet</strong></h4>\n\n<p>You should avoid having an over-sized wallet. Don&rsquo;t think that you need to buy a big wallet because you have a lot to carry with you. In addition, a fat wallet is very ugly. It will make it harder for you to slide the wallet into your trousers&rsquo; pocket. In addition, it will create a bulge and ruin your look.</p>\n\n<p>Before you go on to buy a new wallet, clean out your wallet and place all of the items from your wallet on a table. Throw away things that you would never need any more such as the old bills or the expired gift cards. Remember to check your wallet on a frequent basis to get rid of all of the old stuff that you don&rsquo;t need anymore.</p>\n\n<p style=\"text-align:center\"><img alt=\"f1\" src=\"/storage/news/06.jpg\" /></p>\n\n<p><br />\n&nbsp;</p>\n\n<hr />\n<h4><strong>Number 3: Don&rsquo;t Limit Your Options Of Materials</strong></h4>\n\n<p>The types and designs of wallets are not the only things that you should consider when you go out searching for your best wallet. You have more than 1 option of material rather than leather to choose from as well.</p>\n\n<p>You can experiment with other available options such as cotton, polyester and canvas. They all have their own pros and cons. As a result, they will be suitable for different needs and requirements. You should think about them all in order to choose the material which you would like the most.</p>\n\n<p>&nbsp;</p>\n','published',1,'Botble\\ACL\\Models\\User',1,'news/06.jpg',2252,NULL,'2024-02-24 01:24:50','2024-02-24 01:24:50');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts_translations`
--

DROP TABLE IF EXISTS `posts_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `posts_translations` (
  `lang_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posts_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`lang_code`,`posts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts_translations`
--

LOCK TABLES `posts_translations` WRITE;
/*!40000 ALTER TABLE `posts_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `posts_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `revisions`
--

DROP TABLE IF EXISTS `revisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `revisions` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `revisionable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revisionable_id` bigint unsigned NOT NULL,
  `user_id` bigint unsigned DEFAULT NULL,
  `key` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `old_value` text COLLATE utf8mb4_unicode_ci,
  `new_value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `revisions_revisionable_id_revisionable_type_index` (`revisionable_id`,`revisionable_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `revisions`
--

LOCK TABLES `revisions` WRITE;
/*!40000 ALTER TABLE `revisions` DISABLE KEYS */;
/*!40000 ALTER TABLE `revisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_users`
--

DROP TABLE IF EXISTS `role_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `role_users` (
  `user_id` bigint unsigned NOT NULL,
  `role_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_users_user_id_index` (`user_id`),
  KEY `role_users_role_id_index` (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_users`
--

LOCK TABLES `role_users` WRITE;
/*!40000 ALTER TABLE `role_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `role_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_default` tinyint unsigned NOT NULL DEFAULT '0',
  `created_by` bigint unsigned NOT NULL,
  `updated_by` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`),
  KEY `roles_created_by_index` (`created_by`),
  KEY `roles_updated_by_index` (`updated_by`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Admin','{\"users.index\":true,\"users.create\":true,\"users.edit\":true,\"users.destroy\":true,\"roles.index\":true,\"roles.create\":true,\"roles.edit\":true,\"roles.destroy\":true,\"core.system\":true,\"core.manage.license\":true,\"extensions.index\":true,\"media.index\":true,\"files.index\":true,\"files.create\":true,\"files.edit\":true,\"files.trash\":true,\"files.destroy\":true,\"folders.index\":true,\"folders.create\":true,\"folders.edit\":true,\"folders.trash\":true,\"folders.destroy\":true,\"settings.index\":true,\"settings.options\":true,\"settings.email\":true,\"settings.media\":true,\"settings.cronjob\":true,\"settings.admin-appearance\":true,\"settings.cache\":true,\"settings.datatables\":true,\"settings.email.rules\":true,\"menus.index\":true,\"menus.create\":true,\"menus.edit\":true,\"menus.destroy\":true,\"optimize.settings\":true,\"pages.index\":true,\"pages.create\":true,\"pages.edit\":true,\"pages.destroy\":true,\"plugins.index\":true,\"plugins.edit\":true,\"plugins.remove\":true,\"plugins.marketplace\":true,\"core.appearance\":true,\"theme.index\":true,\"theme.activate\":true,\"theme.remove\":true,\"theme.options\":true,\"theme.custom-css\":true,\"theme.custom-js\":true,\"theme.custom-html\":true,\"widgets.index\":true,\"analytics.general\":true,\"analytics.page\":true,\"analytics.browser\":true,\"analytics.referrer\":true,\"analytics.settings\":true,\"audit-log.index\":true,\"audit-log.destroy\":true,\"backups.index\":true,\"backups.create\":true,\"backups.restore\":true,\"backups.destroy\":true,\"plugins.blog\":true,\"posts.index\":true,\"posts.create\":true,\"posts.edit\":true,\"posts.destroy\":true,\"categories.index\":true,\"categories.create\":true,\"categories.edit\":true,\"categories.destroy\":true,\"tags.index\":true,\"tags.create\":true,\"tags.edit\":true,\"tags.destroy\":true,\"blog.settings\":true,\"plugins.captcha\":true,\"captcha.settings\":true,\"contacts.index\":true,\"contacts.edit\":true,\"contacts.destroy\":true,\"contact.settings\":true,\"galleries.index\":true,\"galleries.create\":true,\"galleries.edit\":true,\"galleries.destroy\":true,\"room.index\":true,\"room.create\":true,\"room.edit\":true,\"room.destroy\":true,\"amenity.index\":true,\"amenity.create\":true,\"amenity.edit\":true,\"amenity.destroy\":true,\"food.index\":true,\"food.create\":true,\"food.edit\":true,\"food.destroy\":true,\"food-type.index\":true,\"food-type.create\":true,\"food-type.edit\":true,\"food-type.destroy\":true,\"booking.index\":true,\"booking.edit\":true,\"booking.destroy\":true,\"booking.reports.index\":true,\"booking-address.index\":true,\"booking-address.create\":true,\"booking-address.edit\":true,\"booking-address.destroy\":true,\"booking-room.index\":true,\"booking-room.create\":true,\"booking-room.edit\":true,\"booking-room.destroy\":true,\"customer.index\":true,\"customer.create\":true,\"customer.edit\":true,\"customer.destroy\":true,\"room-category.index\":true,\"room-category.create\":true,\"room-category.edit\":true,\"room-category.destroy\":true,\"feature.index\":true,\"feature.create\":true,\"feature.edit\":true,\"feature.destroy\":true,\"service.index\":true,\"service.create\":true,\"service.edit\":true,\"service.destroy\":true,\"place.index\":true,\"place.create\":true,\"place.edit\":true,\"place.destroy\":true,\"tax.index\":true,\"tax.create\":true,\"tax.edit\":true,\"tax.destroy\":true,\"invoice.template\":true,\"coupons.index\":true,\"coupons.create\":true,\"coupons.edit\":true,\"coupons.destroy\":true,\"hotel.settings\":true,\"languages.index\":true,\"languages.create\":true,\"languages.edit\":true,\"languages.destroy\":true,\"newsletter.index\":true,\"newsletter.destroy\":true,\"newsletter.settings\":true,\"payment.index\":true,\"payments.settings\":true,\"payment.destroy\":true,\"testimonial.index\":true,\"testimonial.create\":true,\"testimonial.edit\":true,\"testimonial.destroy\":true,\"plugins.translation\":true,\"translations.locales\":true,\"translations.theme-translations\":true,\"translations.index\":true}','Admin users role',1,2,2,'2024-02-24 01:25:02','2024-02-24 01:25:02');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `settings` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=59 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (2,'api_enabled','0',NULL,'2024-02-24 01:25:04'),(3,'activated_plugins','[\"language\",\"language-advanced\",\"analytics\",\"audit-log\",\"backup\",\"blog\",\"captcha\",\"contact\",\"cookie-consent\",\"gallery\",\"hotel\",\"newsletter\",\"payment\",\"paypal\",\"paystack\",\"razorpay\",\"sslcommerz\",\"stripe\",\"testimonial\",\"translation\"]',NULL,'2024-02-24 01:25:04'),(6,'language_hide_default','1',NULL,'2024-02-24 01:25:04'),(8,'language_display','all',NULL,'2024-02-24 01:25:04'),(9,'language_hide_languages','[]',NULL,'2024-02-24 01:25:04'),(10,'media_random_hash','6d2fb4a682cca748257945afc73572f9',NULL,'2024-02-24 01:25:04'),(11,'theme','miranda',NULL,'2024-02-24 01:25:04'),(12,'show_admin_bar','1',NULL,'2024-02-24 01:25:04'),(13,'language_switcher_display','dropdown',NULL,'2024-02-24 01:25:04'),(14,'admin_favicon','general/favicon.png',NULL,'2024-02-24 01:25:04'),(15,'admin_logo','general/logo-white.png',NULL,'2024-02-24 01:25:04'),(16,'permalink-botble-blog-models-post','news',NULL,'2024-02-24 01:25:04'),(17,'permalink-botble-blog-models-category','news',NULL,'2024-02-24 01:25:04'),(18,'payment_cod_status','1',NULL,'2024-02-24 01:25:04'),(19,'payment_cod_description','Please pay money directly to the postman, if you choose cash on delivery method (COD).',NULL,'2024-02-24 01:25:04'),(20,'payment_bank_transfer_status','1',NULL,'2024-02-24 01:25:04'),(21,'payment_bank_transfer_description','Please send money to our bank account: ACB - 69270 213 19.',NULL,'2024-02-24 01:25:04'),(22,'payment_stripe_payment_type','stripe_checkout',NULL,'2024-02-24 01:25:04'),(23,'theme-miranda-site_title','Hotel Miranda',NULL,NULL),(24,'theme-miranda-copyright','©2024 Miranda. All right reserved.',NULL,NULL),(25,'theme-miranda-cookie_consent_message','Your experience on this site will be improved by allowing cookies ',NULL,NULL),(26,'theme-miranda-cookie_consent_learn_more_url','/cookie-policy',NULL,NULL),(27,'theme-miranda-cookie_consent_learn_more_text','Cookie Policy',NULL,NULL),(28,'theme-miranda-homepage_id','1',NULL,NULL),(29,'theme-miranda-blog_page_id','2',NULL,NULL),(30,'theme-miranda-logo','general/logo.png',NULL,NULL),(31,'theme-miranda-logo_white','general/logo-white.png',NULL,NULL),(32,'theme-miranda-favicon','general/favicon.png',NULL,NULL),(33,'theme-miranda-email','info@webmail.com',NULL,NULL),(34,'theme-miranda-address','14/A, Miranda City, NYC',NULL,NULL),(35,'theme-miranda-hotline','+908 987 877 09',NULL,NULL),(36,'theme-miranda-news_banner','general/banner-news.jpg',NULL,NULL),(37,'theme-miranda-rooms_banner','general/banner-news.jpg',NULL,NULL),(38,'theme-miranda-term_of_use_url','#',NULL,NULL),(39,'theme-miranda-privacy_policy_url','#',NULL,NULL),(40,'theme-miranda-preloader_enabled','no',NULL,NULL),(41,'theme-miranda-about-us','Lorem ipsum dolor sit amet, consect etur adipisicing elit, sed doing eius mod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitat ion ullamco laboris nisi.',NULL,NULL),(42,'theme-miranda-hotel_rules','<ul><li>No smoking, parties or events.</li><li>Check-in time from 2 PM, check-out by 10 AM.</li><li>Time to time car parking</li><li>Download Our minimal app</li><li>Browse regular our website</li></ul>',NULL,NULL),(43,'theme-miranda-cancellation','<p>Phasellus volutpat neque a tellus venenatis, a euismod augue facilisis. Fusce ut metus mattis, consequat metus nec, luctus lectus. Pellentesque orci quis hendrerit sed eu dolor. <strong>Cancel up</strong> to <strong>14 days</strong> to get a full refund.</p>',NULL,NULL),(44,'theme-miranda-slider-image-1','sliders/04.jpg',NULL,NULL),(45,'theme-miranda-slider-title-1','The ultimate luxury experience',NULL,NULL),(46,'theme-miranda-slider-description-1','<p>The Perfect<br>Base For You</p>',NULL,NULL),(47,'theme-miranda-slider-primary-button-text-1','Take A tour',NULL,NULL),(48,'theme-miranda-slider-primary-button-url-1','/rooms',NULL,NULL),(49,'theme-miranda-slider-secondary-button-text-1','Learn more',NULL,NULL),(50,'theme-miranda-slider-secondary-button-url-1','/about-us',NULL,NULL),(51,'theme-miranda-slider-image-2','sliders/05.jpg',NULL,NULL),(52,'theme-miranda-slider-title-2','The ultimate luxury experience',NULL,NULL),(53,'theme-miranda-slider-description-2','<p>The Perfect<br>Base For You</p>',NULL,NULL),(54,'theme-miranda-slider-primary-button-text-2','Take A tour',NULL,NULL),(55,'theme-miranda-slider-primary-button-url-2','/rooms',NULL,NULL),(56,'theme-miranda-slider-secondary-button-text-2','Learn more',NULL,NULL),(57,'theme-miranda-slider-secondary-button-url-2','/about-us',NULL,NULL),(58,'theme-miranda-social_links','[[{\"key\":\"social-name\",\"value\":\"Facebook\"},{\"key\":\"social-icon\",\"value\":\"fab fa-facebook-f\"},{\"key\":\"social-url\",\"value\":\"https:\\/\\/www.facebook.com\\/\"}],[{\"key\":\"social-name\",\"value\":\"Twitter\"},{\"key\":\"social-icon\",\"value\":\"fab fa-twitter\"},{\"key\":\"social-url\",\"value\":\"https:\\/\\/www.twitter.com\\/\"}],[{\"key\":\"social-name\",\"value\":\"Youtube\"},{\"key\":\"social-icon\",\"value\":\"fab fa-youtube\"},{\"key\":\"social-url\",\"value\":\"https:\\/\\/www.youtube.com\\/\"}],[{\"key\":\"social-name\",\"value\":\"Behance\"},{\"key\":\"social-icon\",\"value\":\"fab fa-behance\"},{\"key\":\"social-url\",\"value\":\"https:\\/\\/www.behance.com\\/\"}],[{\"key\":\"social-name\",\"value\":\"Linkedin\"},{\"key\":\"social-icon\",\"value\":\"fab fa-linkedin\"},{\"key\":\"social-url\",\"value\":\"https:\\/\\/www.linkedin.com\\/\"}]]',NULL,NULL);
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slugs`
--

DROP TABLE IF EXISTS `slugs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slugs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_id` bigint unsigned NOT NULL,
  `reference_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `slugs_reference_id_index` (`reference_id`),
  KEY `slugs_key_index` (`key`),
  KEY `slugs_prefix_index` (`prefix`),
  KEY `slugs_reference_index` (`reference_id`,`reference_type`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slugs`
--

LOCK TABLES `slugs` WRITE;
/*!40000 ALTER TABLE `slugs` DISABLE KEYS */;
INSERT INTO `slugs` VALUES (1,'general',1,'Botble\\Blog\\Models\\Category','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(2,'hotel',2,'Botble\\Blog\\Models\\Category','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(3,'booking',3,'Botble\\Blog\\Models\\Category','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(4,'resort',4,'Botble\\Blog\\Models\\Category','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(5,'travel',5,'Botble\\Blog\\Models\\Category','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(6,'general',1,'Botble\\Blog\\Models\\Tag','tag','2024-02-24 01:24:50','2024-02-24 01:24:50'),(7,'hotel',2,'Botble\\Blog\\Models\\Tag','tag','2024-02-24 01:24:50','2024-02-24 01:24:50'),(8,'booking',3,'Botble\\Blog\\Models\\Tag','tag','2024-02-24 01:24:50','2024-02-24 01:24:50'),(9,'resort',4,'Botble\\Blog\\Models\\Tag','tag','2024-02-24 01:24:50','2024-02-24 01:24:50'),(10,'travel',5,'Botble\\Blog\\Models\\Tag','tag','2024-02-24 01:24:50','2024-02-24 01:24:50'),(11,'each-of-our-8-double-rooms-has-its-own-distinct',1,'Botble\\Blog\\Models\\Post','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(12,'essential-qualities-of-highly-successful-music',2,'Botble\\Blog\\Models\\Post','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(13,'9-things-i-love-about-shaving-my-head',3,'Botble\\Blog\\Models\\Post','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(14,'why-teamwork-really-makes-the-dream-work',4,'Botble\\Blog\\Models\\Post','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(15,'the-world-caters-to-average-people',5,'Botble\\Blog\\Models\\Post','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(16,'the-litigants-on-the-screen-are-not-actors',6,'Botble\\Blog\\Models\\Post','news','2024-02-24 01:24:50','2024-02-24 01:25:02'),(17,'luxury-hall-of-fame',1,'Botble\\Hotel\\Models\\Room','rooms','2024-02-24 01:24:51','2024-02-24 01:24:51'),(18,'pendora-fame',2,'Botble\\Hotel\\Models\\Room','rooms','2024-02-24 01:24:51','2024-02-24 01:24:51'),(19,'pacific-room',3,'Botble\\Hotel\\Models\\Room','rooms','2024-02-24 01:24:51','2024-02-24 01:24:51'),(20,'junior-suite',4,'Botble\\Hotel\\Models\\Room','rooms','2024-02-24 01:24:51','2024-02-24 01:24:51'),(21,'family-suite',5,'Botble\\Hotel\\Models\\Room','rooms','2024-02-24 01:24:51','2024-02-24 01:24:51'),(22,'relax-suite',6,'Botble\\Hotel\\Models\\Room','rooms','2024-02-24 01:24:51','2024-02-24 01:24:51'),(23,'luxury-suite',7,'Botble\\Hotel\\Models\\Room','rooms','2024-02-24 01:24:51','2024-02-24 01:24:51'),(24,'president-room',8,'Botble\\Hotel\\Models\\Room','rooms','2024-02-24 01:24:51','2024-02-24 01:24:51'),(25,'duplex-restaurant',1,'Botble\\Hotel\\Models\\Place','places','2024-02-24 01:24:59','2024-02-24 01:24:59'),(26,'duplex-restaurant',2,'Botble\\Hotel\\Models\\Place','places','2024-02-24 01:24:59','2024-02-24 01:24:59'),(27,'duplex-restaurant',3,'Botble\\Hotel\\Models\\Place','places','2024-02-24 01:24:59','2024-02-24 01:24:59'),(28,'duplex-restaurant',4,'Botble\\Hotel\\Models\\Place','places','2024-02-24 01:24:59','2024-02-24 01:24:59'),(29,'duplex-restaurant',5,'Botble\\Hotel\\Models\\Place','places','2024-02-24 01:24:59','2024-02-24 01:24:59'),(30,'duplex-restaurant',6,'Botble\\Hotel\\Models\\Place','places','2024-02-24 01:24:59','2024-02-24 01:24:59'),(31,'homepage',1,'Botble\\Page\\Models\\Page','','2024-02-24 01:24:59','2024-02-24 01:24:59'),(32,'news',2,'Botble\\Page\\Models\\Page','','2024-02-24 01:24:59','2024-02-24 01:24:59'),(33,'contact',3,'Botble\\Page\\Models\\Page','','2024-02-24 01:24:59','2024-02-24 01:24:59'),(34,'restaurant',4,'Botble\\Page\\Models\\Page','','2024-02-24 01:24:59','2024-02-24 01:24:59'),(35,'our-gallery',5,'Botble\\Page\\Models\\Page','','2024-02-24 01:24:59','2024-02-24 01:24:59'),(36,'about-us',6,'Botble\\Page\\Models\\Page','','2024-02-24 01:24:59','2024-02-24 01:24:59'),(37,'places',7,'Botble\\Page\\Models\\Page','','2024-02-24 01:24:59','2024-02-24 01:24:59'),(38,'our-offers',8,'Botble\\Page\\Models\\Page','','2024-02-24 01:24:59','2024-02-24 01:24:59'),(39,'cookie-policy',9,'Botble\\Page\\Models\\Page','','2024-02-24 01:24:59','2024-02-24 01:24:59'),(40,'duplex-restaurant',1,'Botble\\Gallery\\Models\\Gallery','galleries','2024-02-24 01:25:01','2024-02-24 01:25:01'),(41,'luxury-room',2,'Botble\\Gallery\\Models\\Gallery','galleries','2024-02-24 01:25:01','2024-02-24 01:25:01'),(42,'pacific-room',3,'Botble\\Gallery\\Models\\Gallery','galleries','2024-02-24 01:25:01','2024-02-24 01:25:01'),(43,'family-room',4,'Botble\\Gallery\\Models\\Gallery','galleries','2024-02-24 01:25:01','2024-02-24 01:25:01'),(44,'king-bed',5,'Botble\\Gallery\\Models\\Gallery','galleries','2024-02-24 01:25:01','2024-02-24 01:25:01'),(45,'special-foods',6,'Botble\\Gallery\\Models\\Gallery','galleries','2024-02-24 01:25:01','2024-02-24 01:25:01'),(46,'wifi',1,'Botble\\Hotel\\Models\\Service','services','2024-02-24 01:25:05','2024-02-24 01:25:05'),(47,'car-rental',2,'Botble\\Hotel\\Models\\Service','services','2024-02-24 01:25:05','2024-02-24 01:25:05'),(48,'satellite-tv',3,'Botble\\Hotel\\Models\\Service','services','2024-02-24 01:25:05','2024-02-24 01:25:05'),(49,'sea-view',4,'Botble\\Hotel\\Models\\Service','services','2024-02-24 01:25:05','2024-02-24 01:25:05'),(50,'laundry',5,'Botble\\Hotel\\Models\\Service','services','2024-02-24 01:25:05','2024-02-24 01:25:05'),(51,'breakfast',6,'Botble\\Hotel\\Models\\Service','services','2024-02-24 01:25:05','2024-02-24 01:25:05');
/*!40000 ALTER TABLE `slugs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slugs_translations`
--

DROP TABLE IF EXISTS `slugs_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `slugs_translations` (
  `lang_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slugs_id` bigint unsigned NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `prefix` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT '',
  PRIMARY KEY (`lang_code`,`slugs_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slugs_translations`
--

LOCK TABLES `slugs_translations` WRITE;
/*!40000 ALTER TABLE `slugs_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `slugs_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tags` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author_id` bigint unsigned DEFAULT NULL,
  `author_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Botble\\ACL\\Models\\User',
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT '',
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'General',1,'Botble\\ACL\\Models\\User','','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(2,'Hotel',1,'Botble\\ACL\\Models\\User','','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(3,'Booking',1,'Botble\\ACL\\Models\\User','','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(4,'Resort',1,'Botble\\ACL\\Models\\User','','published','2024-02-24 01:24:50','2024-02-24 01:24:50'),(5,'Travel',1,'Botble\\ACL\\Models\\User','','published','2024-02-24 01:24:50','2024-02-24 01:24:50');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags_translations`
--

DROP TABLE IF EXISTS `tags_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tags_translations` (
  `lang_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tags_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(400) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lang_code`,`tags_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags_translations`
--

LOCK TABLES `tags_translations` WRITE;
/*!40000 ALTER TABLE `tags_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `tags_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials`
--

DROP TABLE IF EXISTS `testimonials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testimonials` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'published',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials`
--

LOCK TABLES `testimonials` WRITE;
/*!40000 ALTER TABLE `testimonials` DISABLE KEYS */;
INSERT INTO `testimonials` VALUES (1,'Adam Williams','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua','testimonials/01.png','CEO Of Microsoft','published','2024-02-24 01:25:00','2024-02-24 01:25:00'),(2,'Retha Deowalim','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua','testimonials/02.png','CEO Of Apple','published','2024-02-24 01:25:00','2024-02-24 01:25:00'),(3,'Sam J. Wasim','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua','testimonials/03.png','Pio Founder','published','2024-02-24 01:25:00','2024-02-24 01:25:00');
/*!40000 ALTER TABLE `testimonials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `testimonials_translations`
--

DROP TABLE IF EXISTS `testimonials_translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testimonials_translations` (
  `lang_code` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `testimonials_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `company` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`lang_code`,`testimonials_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testimonials_translations`
--

LOCK TABLES `testimonials_translations` WRITE;
/*!40000 ALTER TABLE `testimonials_translations` DISABLE KEYS */;
/*!40000 ALTER TABLE `testimonials_translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_meta`
--

DROP TABLE IF EXISTS `user_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_meta` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_meta_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_meta`
--

LOCK TABLES `user_meta` WRITE;
/*!40000 ALTER TABLE `user_meta` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `first_name` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(120) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_id` bigint unsigned DEFAULT NULL,
  `super_user` tinyint(1) NOT NULL DEFAULT '0',
  `manage_supers` tinyint(1) NOT NULL DEFAULT '0',
  `permissions` text COLLATE utf8mb4_unicode_ci,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_username_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'volkman.brandy@rau.com',NULL,'$2y$12$goW5zxH9g3g9Tg68DXXVRuDnp1SmgZ.qZ5hti2TLw31.15DxyFNZi',NULL,'2024-02-24 01:25:02','2024-02-24 01:25:02','Jazmin','Blick','botble',NULL,1,1,NULL,NULL),(2,'johnpaul.rippin@heaney.com',NULL,'$2y$12$39Wo2jr0rUHjUxTehB3cyuC3LxmX1LHGnxnD7K72Fw3pCMgUpCEIS',NULL,'2024-02-24 01:25:02','2024-02-24 01:25:02','Enola','Bartoletti','admin',NULL,1,1,NULL,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `widgets`
--

DROP TABLE IF EXISTS `widgets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `widgets` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `widget_id` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sidebar_id` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` tinyint unsigned NOT NULL DEFAULT '0',
  `data` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `widgets`
--

LOCK TABLES `widgets` WRITE;
/*!40000 ALTER TABLE `widgets` DISABLE KEYS */;
INSERT INTO `widgets` VALUES (1,'CustomMenuWidget','footer_sidebar','miranda',0,'{\"id\":\"CustomMenuWidget\",\"name\":\"Services.\",\"menu_id\":\"services\"}','2024-02-24 01:25:04','2024-02-24 01:25:04'),(2,'RecentPostsWidget','primary_sidebar','miranda',0,'{\"id\":\"RecentPostsWidget\",\"name\":\"Recent posts\",\"number_display\":5}','2024-02-24 01:25:04','2024-02-24 01:25:04'),(3,'BlogCategoriesWidget','primary_sidebar','miranda',1,'{\"id\":\"BlogCategoriesWidget\",\"name\":\"Categories\",\"number_display\":5}','2024-02-24 01:25:04','2024-02-24 01:25:04'),(4,'BlogTagsWidget','primary_sidebar','miranda',2,'{\"id\":\"BlogTagsWidget\",\"name\":\"Popular Tags\",\"number_display\":5}','2024-02-24 01:25:04','2024-02-24 01:25:04');
/*!40000 ALTER TABLE `widgets` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-02-24 15:25:06
