<?php

namespace Botble\Hotel\Http\Requests\Fronts;

use Botble\Hotel\Facades\HotelHelper;
use Botble\Support\Http\Requests\Request;

class RoomListRequest extends Request
{
    public function rules(): array
    {
        $dateFormat = HotelHelper::getDateFormat();

        return [
            'start_date' => ['nullable', 'string', 'date_format:' . $dateFormat, 'after_or_equal:today'],
            'end_date' => ['nullable', 'string', 'date_format:' . $dateFormat, 'after_or_equal:start_date'],
            'adults' => ['nullable', 'integer', 'min:1'],
        ];
    }
}
